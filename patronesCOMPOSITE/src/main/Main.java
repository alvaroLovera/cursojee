package main;

public class Main {

	public static void main(String[] args) {

		Rama ramaPrincipal = new Rama("pedido");
		Rama identificarUsduario = new Rama("identificar Usduario");
		Rama procesarCompra = new Rama("procesar Compra");
		
		identificarUsduario.add(new Hoja("Mostrar formulario"));
		identificarUsduario.add(new Hoja("Comprovar datos insertados"));
		ramaPrincipal.add(identificarUsduario);
		ramaPrincipal.add(procesarCompra);
		ramaPrincipal.mostrar();
	}

}
