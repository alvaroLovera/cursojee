package modelo;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class Anuncio {
	
	@NotEmpty(message = "Titulo no puede estar vacio")
	@Pattern(regexp = "[a-zA-Z]{3,20}", message = "Titulo no valido")
	private String titulo;
	@NotEmpty(message = "Email no puede estar vacio")	
	private String email;
	@Size(min = 1, max = 300, message = "Descripcion debe tener entre 1 y 300 caracteres")
	private String descripcion;
	@DecimalMin(value = "1.0", message = "El valor minimo es de 1.0")
	private double precio;
	@NotEmpty
	@Pattern(regexp = "[0-9]{3,9}", message = "Telefono no valido")
	private String telefono;
	@NotEmpty
	private String ciudad;
	@NotEmpty(message = "Debe seleccionar un valor")
	private String estado;
	
	CommonsMultipartFile fichero;
	
	private int id;
	
	public Anuncio() {
	}

	public Anuncio(String titulo, String email, String descripcion, double precio, String telefono, String ciudad,
			String estado) {
		this.titulo = titulo;
		this.email = email;
		this.descripcion = descripcion;
		this.precio = precio;
		this.telefono = telefono;
		this.ciudad = ciudad;
		this.estado = estado;
	}

	public Anuncio(String titulo, String email, String descripcion, double precio, String telefono, String ciudad,
			String estado, int id) {
		this.titulo = titulo;
		this.email = email;
		this.descripcion = descripcion;
		this.precio = precio;
		this.telefono = telefono;
		this.ciudad = ciudad;
		this.estado = estado;
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public CommonsMultipartFile getFichero() {
		return fichero;
	}

	public void setFichero(CommonsMultipartFile fichero) {
		this.fichero = fichero;
	}

	@Override
	public String toString() {
		return "Anuncio [titulo=" + titulo + ", email=" + email + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", telefono=" + telefono + ", ciudad=" + ciudad + ", estado=" + estado + ", fichero=" + fichero
				+ ", id=" + id + "]";
	}

	
	
}
