package modelo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class Servicio {

	private int id;
	@NotEmpty(message = "Rellene el campo nombre")
	private String nombre;
	@NotEmpty(message = "Rellene el campo profesion")
	private String profesion;
	@NotEmpty(message = "Rellene el campo tarea")
	private String tarea;
	@NotEmpty(message = "El campo descripcion debe tener minimo 3 caracteres y maximo 300")
	@Size(max = 300, min = 1)
	private String descripcion;
	@NotEmpty(message = "Rellene el campo ciudad")
	private String ciudad;
	
	public Servicio() {
	}

	public Servicio(String nombre, String profesion, String tarea, String descripcion, String ciudad) {
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
	}

	public Servicio(int id, String nombre, String profesion, String tarea, String descripcion, String ciudad) {
		this.id = id;
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getTarea() {
		return tarea;
	}

	public void setTarea(String tarea) {
		this.tarea = tarea;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Override
	public String toString() {
		return "Servicio [id=" + id + ", nombre=" + nombre + ", profesion=" + profesion + ", tarea=" + tarea
				+ ", descripcion=" + descripcion + ", ciudad=" + ciudad + "]";
	}
	
	
}
