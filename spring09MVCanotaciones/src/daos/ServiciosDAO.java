package daos;

import java.util.List;

import modelo.Servicio;


public interface ServiciosDAO {
	public void registrarServicio(Servicio s);
	public List<Servicio> obtenerServicios();
}
