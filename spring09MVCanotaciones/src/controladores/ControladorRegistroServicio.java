package controladores;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.ServiciosDAO;
import daosImpl.ServiciosDAOImpl;
import modelo.Anuncio;
import modelo.Servicio;

@Controller
public class ControladorRegistroServicio {

	@Autowired
	private ServiciosDAO serviciosDAO;
	@Autowired
	private ControlladorServiciosInicio controladorServicioInicio;

	@RequestMapping("prepararServicio")
	public String prepararServicio(Map model) {		
		model.put("nuevoServicio",new Servicio());		
		return "registroServicio";
	}
	

	@RequestMapping("guardarServicio")
	public String guardarServicio( @ModelAttribute("nuevoServicio") @Valid Servicio nuevoServicio, BindingResult result, Map model) {

		System.out.println("he recibido: " + nuevoServicio.toString());
		if (result.hasErrors()) {
			//Hay que volver a mandar un objeto al form con lo que queramos que muestre
			model.put("nuevoServicio", nuevoServicio);
			return "registrarAnuncio";
		} else {
			serviciosDAO.registrarServicio(nuevoServicio);
			System.out.println("anuncio registrado ok");
			return controladorServicioInicio.serviciosInicio(model);
		}
	}
}
