package controladores;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.ServiciosDAO;
import modelo.Anuncio;
import modelo.Servicio;

@Controller
public class ControlladorServiciosInicio {
	
	@Autowired
	private ServiciosDAO serviciosDAO;
	
	

	@RequestMapping("serviciosInicio")
	public String serviciosInicio(Map model) {
		System.out.println("se ejecuta el metodo serviciosInicio");
		
		List<Servicio> servicios = serviciosDAO.obtenerServicios();
		model.put("servicios", servicios);
		// Lo siguiente lleva a la vista inicio.jsp
		// de las vistas gracias a la bean que acabammos de insertar en el 
		// dispatcher-servlet.xml
		return "servicios";
	}
}
