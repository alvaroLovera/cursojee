package daosImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import constantes.ConstantesSQL;
import constantes.NombresTablas;
import daos.AnunciosDAO;
import mappers.AnuncioMapper;
import modelo.Anuncio;

@Component
public class AnunciosDAOImpl implements AnunciosDAO{

	//a diferencia de otros DAO realizados anteriormente
	//vasmo a usar spring jdbc para facilitarnos la tarea
	//con bases de datos. Para ello la clase actual necesita
	//un DataSource. Un datasource no es mas que un elemento
	//que tiene informacion para poder conectarse con una 
	//base de datos.
	@Autowired
	private DataSource miDataSource;
	private SimpleJdbcInsert simpleInsert;
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int registrarAnuncio(Anuncio a) {
		//vamos a usar el simple jdbc insert, que es un recurso
		//de spring jdbc, el cual ha necesitado de un data source
		int idGenerado = -1;
		Map<String, Object> valores = new HashMap<String, Object>();
		valores.put("titulo", a.getTitulo());
		valores.put("email", a.getEmail());
		valores.put("descripcion", a.getDescripcion());
		valores.put("precio", a.getPrecio());
		valores.put("telefono", a.getTelefono());
		valores.put("ciudad", a.getCiudad());
		valores.put("estado", a.getEstado());
		idGenerado = simpleInsert.usingGeneratedKeyColumns("id").executeAndReturnKey(valores).intValue();
		
		return idGenerado;
	}

	@Override
	public List<Anuncio> obntenerAnuncios() {
		// Spring jdbc nos pide ne una clase tipo Mapper
		// como transformar de un resultado de BBDD
		// de la tabla_anuncios a un objeto de la clase Anuncios
		// Una vez tengamos esa clase va a ser muy facil obtener datos
		
		List<Anuncio> anuncios = jdbcTemplate.query(ConstantesSQL.SQL_OBTENER_ANUNCIOS, new AnuncioMapper());	
		return anuncios;
	}
	
	@PostConstruct
	public void setMiDataSource() {
		simpleInsert = new SimpleJdbcInsert(miDataSource);
		simpleInsert.setTableName(NombresTablas.TABLA_ANUNCIOS);
		jdbcTemplate = new JdbcTemplate(miDataSource);
	}

	

}
