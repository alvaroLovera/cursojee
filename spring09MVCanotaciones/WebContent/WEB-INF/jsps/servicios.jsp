<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a href="prepararServicio">REGISTRAR MI Servicio</a>
<a href="index.jsp" >Inicio</a>
<br/>
Listado de servicios:<br/>
<c:forEach items="${servicios}" var="servicio" >
<div>
-------------------------------<br/>
Nombre: ${servicio.nombre} <br/>
Profesion:  ${servicio.profesion} <br/>
Tarea:  ${servicio.tarea}<br/>
Descripcion: ${servicio.descripcion}<br/>
Ciudad: ${servicio.ciudad}<br/>
-------------------------------<br/>
</div>
</c:forEach>
</body>
</html>