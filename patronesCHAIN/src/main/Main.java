package main;

public class Main {

	public static void main(String[] args) {
		
		// Definicion de los elementos de la caena
		Director ana = new Director();
		
		Vicepresidente luis = new  Vicepresidente();
		
		Presidente mario = new Presidente();
		// Montar los elementos de la cadena
		ana.setSucesor(luis);
		luis.setSucesor(mario);
		// La cadena procesa peticiones desde el primer elemento
		Compra c = new Compra("suministros", 5000);
		ana.procesarPeticion(c);
		c = new Compra("Proyecto A", 999000);
		ana.procesarPeticion(c);

	}

}
