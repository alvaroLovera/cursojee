package main;

public class Vicepresidente  extends Handler{

	@Override
	public void procesarPeticion(Compra c) {
		if(c.getCantidad() > 100_000) {
			System.out.println("Sucesor procesa peticion");
			sucesor.procesarPeticion(c);
		}else {
			System.out.println("Vicepresidente procesa la compra " + c.getProposito());
		}
		
	}

}
