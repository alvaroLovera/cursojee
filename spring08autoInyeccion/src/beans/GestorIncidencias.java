package beans;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GestorIncidencias {
	
	// Con autowired le deccimos a Spring que busque la primera Bean que encuentre de tipo 
	// GestorUsuarios y se la asigne al campo gestorusuarios que acabamos de definir
	//@Autowired
	//@Qualifier("gestorUsuarios")
	//@Resource
	private GestorUsuarios gestorUsuarios;
	
	
	@Autowired // Tambien puede ir en el constructor consiguiendo el mismo efecto
	public GestorIncidencias(GestorUsuarios gestorUsuarios) {
		super();
		this.gestorUsuarios = gestorUsuarios;
	}

	public void crearIncidencia(int idUsuario) {
		System.out.println("Creando incidencia para usuario " + idUsuario) ;
		gestorUsuarios.notificarUsuario(idUsuario);
	}

	//@Autowired // Autowired puede ir en el setter realizando la misma funcion
	public void setGestorUsuarios(GestorUsuarios gestorUsuarios) {
		this.gestorUsuarios = gestorUsuarios;
	}
	
}
