package beans;

import org.springframework.stereotype.Component;

@Component
public class GestorUsuarios {
	
	public void notificarUsuario(int id) {
		System.out.println("Notificar usuario: " + id);
	}
	
	public void borrarUsuario(int idUsuario) {
		System.out.println("Borrando usuario: " + idUsuario);
		
	}
}
