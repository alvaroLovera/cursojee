package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import daos.NovedadesDAO;
import daos.NovedadesDAOImpl;
import modelo.Novedad;

@WebServlet("/ServletNovedades")
public class ServletNovedades extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		NovedadesDAO novedadesDAO = new NovedadesDAOImpl();
		List<Novedad> novedades = novedadesDAO.obtenerNovedades();
		// Usar gson para obtener el JSON de las novedades
		Gson gson = new Gson();
		String json = gson.toJson(novedades);
		response.getWriter().print(json);
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String json = req.getParameter("json");
		System.out.println(json);
//		resp.getWriter().print(json);
		Gson gson = new Gson();
		Novedad novedad = gson.fromJson(json, Novedad.class);
		System.out.println(novedad.toString());
		NovedadesDAO novedadesDAO = new NovedadesDAOImpl();
		novedadesDAO.registrarNovedad(novedad);
		resp.getWriter().print("Novedad registrada correctamente");
		
	}

	

}
