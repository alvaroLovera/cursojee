package main;

public class Delantero implements Jugador, Cloneable{

	@Override
	public void correr() {
		System.out.println("Delantero corre");
		
	}

	@Override
	public void parar() {
		System.out.println("Delantero para");
		
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public Jugador clonar() {
		try {
			System.out.println("Clonando delantero");
			return (Jugador) clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	
	
}
