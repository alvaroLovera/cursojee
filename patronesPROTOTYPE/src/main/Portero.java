package main;

public class Portero implements Jugador, Cloneable{

	@Override
	public void correr() {
		System.out.println("Portero corre");
		
	}

	@Override
	public void parar() {
		System.out.println("Portero se para");
		
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public Jugador clonar() {
		
		try {
			System.out.println("Clonando portero");
			return (Jugador) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	
	
}
