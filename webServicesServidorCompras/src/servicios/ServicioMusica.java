package servicios;

import java.util.List;

import javax.jws.WebService;

import modelo.Musica;
import modelo.MusicaDAO;
import modelo.MusicaDAOImpl;

@WebService(targetNamespace = "http://servicios/", endpointInterface = "servicios.ServidorServicioMusica", portName = "ServicioMusicaPort", serviceName = "ServicioMusicaService")
public class ServicioMusica implements ServidorServicioMusica {

	private MusicaDAO musicasDAO = new MusicaDAOImpl();
	
	public void registrarMusica(Musica m) {
		
		musicasDAO.registrarMusica(m);
	}
	
	public List<Musica> obtenerMusicas(){
		return musicasDAO.obtenerMusicas();
	}
	
}
