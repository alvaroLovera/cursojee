package servicios;

import java.util.List;

import javax.jws.WebService;

import modelo.Musica;

@WebService(name = "ServidorServicioMusica", targetNamespace = "http://servicios/")
public interface ServidorServicioMusica {

	void registrarMusica(Musica m);

	List<Musica> obtenerMusicas();

}