package modelo;

public class Musica {
	
	private int id;
	private String nombre, cantante, discografica, genero;
	private int duracion;
	private double precio;
	
	public Musica() {
		// TODO Auto-generated constructor stub
	}

	public Musica(int id, String nombre, String cantante, String discografica, String genero, int duracion,
			double precio) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cantante = cantante;
		this.discografica = discografica;
		this.genero = genero;
		this.duracion = duracion;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCantante() {
		return cantante;
	}

	public void setCantante(String cantante) {
		this.cantante = cantante;
	}

	public String getDiscografica() {
		return discografica;
	}

	public void setDiscografica(String discografica) {
		this.discografica = discografica;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Musica [id=" + id + ", nombre=" + nombre + ", cantante=" + cantante + ", discografica=" + discografica
				+ ", genero=" + genero + ", duracion=" + duracion + ", precio=" + precio + "]";
	}
	
	
	
}
