package modelo;

import java.util.List;

public interface MusicaDAO {

	boolean registrarMusica(Musica m);

	List<Musica> obtenerMusicas();

}