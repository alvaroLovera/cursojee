package servlets.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import modelo.UsuariosDAO;
import modelo.UsuariosDAOImpl;

/**
 * Servlet implementation class ServletAdminListarUsuarios
 */
@WebServlet("/admin/ServletAdminListarUsuarios")
public class ServletAdminListarUsuarios extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//obtener el listado de usuarios y darselo 
		//A usuarios.jsp para que los muestre
		UsuariosDAO usuariosDAO = new UsuariosDAOImpl();
		ArrayList<Usuario> usuarios = usuariosDAO.obtenerUsuarios();
		request.setAttribute("usuarios", usuarios);
		
		RequestDispatcher rd = 
				getServletContext().
					getRequestDispatcher("/admin/usuarios.jsp");
		rd.forward(request, response);
	}

}
