package beans;

public class GestorArchivos {
	// Para poder usar un gestor de imagenes desde este gestor de archivos 
	// se hace asi
	private GestorImagenes gImagenes;
	private String rutaRaiz;

	public void borrarArchivo(String archivo) {
		System.out.println("Acrtuand sobre " + rutaRaiz);
		System.out.println("Borrando archivo: " + archivo);
		System.out.println("Brrando imagenes del archivo");
		gImagenes.borrarImagenesDeArchivo(archivo);
	}
	
	public String getRutaRaiz() {
		return rutaRaiz;
	}

	public void setRutaRaiz(String rutaRaiz) {
		this.rutaRaiz = rutaRaiz;
	}

	public GestorImagenes getgImagenes() {
		return gImagenes;
	}

	public void setgImagenes(GestorImagenes gImagenes) {
		this.gImagenes = gImagenes;
	}
	
	
}
