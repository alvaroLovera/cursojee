package daos;

import java.util.List;

import modelo.Noticia;

public interface NoticiaDAO {

	List<Noticia> obtenerNoticias();

	void registrarNoticia(Noticia n);

	void borrarNoticia(Noticia n);

	void editarNoticia();

}