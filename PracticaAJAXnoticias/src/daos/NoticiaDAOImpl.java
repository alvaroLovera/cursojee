package daos;

import java.util.ArrayList;
import java.util.List;

import modelo.Noticia;

public class NoticiaDAOImpl implements NoticiaDAO {

	private List<Noticia> noticias = new ArrayList<Noticia>();
	
	@Override
	public List<Noticia> obtenerNoticias(){
		return noticias;
	}
	
	@Override
	public void registrarNoticia(Noticia n) {
		noticias.add(n);
	}
	
	@Override
	public void borrarNoticia(Noticia n) {
		noticias.remove(n);
	}
	
	@Override
	public void editarNoticia() {
		
	}
}
