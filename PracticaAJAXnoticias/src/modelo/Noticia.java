package modelo;

public class Noticia {
	private String titular;
	private String texto;
	private String tipo;
	private String autor;
	private int id;
	
	public Noticia() {
		// TODO Auto-generated constructor stub
	}

	public Noticia(String titular, String texto, String tipo, String autor) {
		super();
		this.titular = titular;
		this.texto = texto;
		this.tipo = tipo;
		this.autor = autor;
	}

	public Noticia(String titular, String texto, String tipo, String autor, int id) {
		super();
		this.titular = titular;
		this.texto = texto;
		this.tipo = tipo;
		this.autor = autor;
		this.id = id;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Noticia [titular=" + titular + ", texto=" + texto + ", tipo=" + tipo + ", autor=" + autor + ", id=" + id
				+ "]";
	}
	

}
