package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.PeliculasDAO;

public class Principal {

	public static void main(String[] args) {
		
//		Para crar un contenedor de Spring con lo puesto en el aplicationContext.xml
		ApplicationContext contenedor = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		// Cuando se pida un DAO o cualquier clase que implemente una interfaz lo suyo
		// es pedirla por el interfaz:
		PeliculasDAO pDAO = contenedor.getBean(PeliculasDAO.class);
		pDAO.borrarPelicula(11);
		// Ahora mismo hay 3 implementaciones de PeliculasDAO
		// �Cual se va a usar?
		// Pues la que dig en el applicationContext.xml
		// de esta forma este mismo proyecto ofrecido en otro sitio segun lo que diga el xml
		// usara un aimplemntacon u otra
		
		
	}

}
