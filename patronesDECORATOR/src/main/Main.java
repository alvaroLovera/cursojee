package main;

public class Main {

	public static void main(String[] args) {
		InterfazJugador ana = new Jugador();
		// Esta es la forma mas usual de usar decorator
		ana = new EntrenadorPersonal(ana);
		ana.entrenar();
		ana.descansar();
	}

}
