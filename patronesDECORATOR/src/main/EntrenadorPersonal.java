package main;

// Esta es la clase que actua como DECORATOR
public class EntrenadorPersonal implements InterfazJugador {
	
	private InterfazJugador elementoAdecorar;

	public EntrenadorPersonal(InterfazJugador elementoAdecorar) {
		super();
		this.elementoAdecorar = elementoAdecorar;
		
	}

	@Override
	public void entrenar() {
		System.out.println("Entrenador manda entrenar");
		elementoAdecorar.entrenar();
	}

	@Override
	public void descansar() {
		System.out.println("Entrenador manda descanso");
		elementoAdecorar.descansar();
	}
	
	
	
}
