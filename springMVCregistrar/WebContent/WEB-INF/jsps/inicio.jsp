<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

Bienvenido a mi portal de anuncios 
<a href="prepararRegistro">REGISTRAR MI ANUNCIO</a>
<br/>
<a href="inicioServicio" >ACCEDER AL PORTAL</a>
Listado de anuncios:<br/>
<c:forEach items="${anuncios}" var="anuncio" >
<div>
-------------------------------<br/>
Titulo: ${anuncio.titulo} <br/>
Descripcion:  ${anuncio.descripcion} <br/>
Email:  ${anuncio.email}<br/>
Precio: ${anuncio.precio}<br/>
Tlf: ${anuncio.telefono}<br/>
Ciudad: ${anuncio.ciudad}<br/>
-------------------------------<br/>
</div>
</c:forEach>

</body>
</html>