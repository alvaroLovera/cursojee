package modelo;

public class Servicio {

	private int id;
	private String nombre; 
	private String profesion;
	private String tarea;
	private String descripcion;
	private String ciudad;
	
	public Servicio() {
	}

	public Servicio(String nombre, String profesion, String tarea, String descripcion, String ciudad) {
		super();
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
	}

	public Servicio(int id, String nombre, String profesion, String tarea, String descripcion, String ciudad) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getTarea() {
		return tarea;
	}

	public void setTarea(String tarea) {
		this.tarea = tarea;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Override
	public String toString() {
		return "Servicio [id=" + id + ", nombre=" + nombre + ", profesion=" + profesion + ", tarea=" + tarea
				+ ", descripcion=" + descripcion + ", ciudad=" + ciudad + "]";
	}
	
	
}
