package modelo;

public class Anuncio {
	
	private String titulo;
	private String email;
	private String descripcion;
	private double precio;
	private String telefono;
	private String ciudad;	
	private int id;
	
	public Anuncio() {
	}

	public Anuncio(String titulo, String email, String descripcion, double precio, String telefono, String ciudad) {
		super();
		this.titulo = titulo;
		this.email = email;
		this.descripcion = descripcion;
		this.precio = precio;
		this.telefono = telefono;
		this.ciudad = ciudad;
	}

	public Anuncio(String titulo, String email, String descripcion, double precio, String telefono, String ciudad,
			int id) {
		super();
		this.titulo = titulo;
		this.email = email;
		this.descripcion = descripcion;
		this.precio = precio;
		this.telefono = telefono;
		this.ciudad = ciudad;
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Anuncio [titulo=" + titulo + ", email=" + email + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", telefono=" + telefono + ", ciudad=" + ciudad + ", id=" + id + "]";
	}

	
}
