package daosImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import daos.ServiciosDAO;
import modelo.Servicio;

public class ServiciosDAOImpl implements ServiciosDAO{

	private DataSource dataSource;
	private SimpleJdbcInsert simpleInsert;
	private JdbcTemplate template;
	
	
	@Override
	public void registrarServicio(Servicio s) {
				Map<String, Object> valores = 
						new HashMap<String, Object>();
				valores.put("nombre", s.getNombre());
				valores.put("profesion", s.getProfesion());
				valores.put("descripcion", s.getDescripcion());
				valores.put("tarea", s.getTarea());
				valores.put("ciudad", s.getCiudad());
			
				simpleInsert.execute(valores);
		
	}

	@Override
	public List<Servicio> obtenerServicios() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		simpleInsert = new  SimpleJdbcInsert(dataSource);
		simpleInsert.setTableName("tabla_servicios");
		template = new JdbcTemplate(dataSource);
	}

}
