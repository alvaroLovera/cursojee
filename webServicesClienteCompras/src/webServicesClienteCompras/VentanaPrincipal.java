package webServicesClienteCompras;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import servicios.Musica;
import servicios.ServidorServicioMusicaProxy;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField tfNombre;
	private JTextField tfDiscografica;
	private JTextField tfGenero;
	private JTextField tfDuracion;
	private JTextField tfPrecio;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
					
					
					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		
		JScrollPane scrollpane;
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(29, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Cantante");
		lblNewLabel_1.setBounds(29, 47, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Discografica");
		lblNewLabel_2.setBounds(29, 85, 67, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Genero");
		lblNewLabel_3.setBounds(29, 127, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Duracion");
		lblNewLabel_4.setBounds(29, 163, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Precio");
		lblNewLabel_5.setBounds(29, 203, 46, 14);
		contentPane.add(lblNewLabel_5);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(105, 8, 86, 20);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);
		
		JTextField tfCantante = new JTextField();
		tfCantante.setBounds(105, 44, 86, 20);
		contentPane.add(tfCantante);
		tfCantante.setColumns(10);
		
		tfDiscografica = new JTextField();
		tfDiscografica.setBounds(106, 82, 86, 20);
		contentPane.add(tfDiscografica);
		tfDiscografica.setColumns(10);
		
		tfGenero = new JTextField();
		tfGenero.setBounds(105, 124, 86, 20);
		contentPane.add(tfGenero);
		tfGenero.setColumns(10);
		
		tfDuracion = new JTextField();
		tfDuracion.setBounds(105, 160, 86, 20);
		contentPane.add(tfDuracion);
		tfDuracion.setColumns(10);
		
		tfPrecio = new JTextField();
		tfPrecio.setBounds(105, 200, 86, 20);
		contentPane.add(tfPrecio);
		tfPrecio.setColumns(10);
		
		JButton btnGuardar = new JButton("GUARDAR");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombre = tfNombre.getText();
				String cantante = tfCantante.getText();
				String discografica = tfDiscografica.getText();
				String genero = tfGenero.getText();
				String duracionString = tfDuracion.getText();
				String precioString = tfPrecio.getText();
				
				try {
					int duracion = Integer.parseInt(duracionString);
					double precio = Double.parseDouble(precioString);
					ServidorServicioMusicaProxy proxy = new ServidorServicioMusicaProxy();
					Musica m = new Musica(cantante, discografica, duracion, genero, 3, nombre, precio);
					proxy.registrarMusica(m);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				
				
			}
		});
		btnGuardar.setBounds(55, 227, 89, 23);
		contentPane.add(btnGuardar);
		
		table = new JTable();
		table.setBounds(201, 11, 223, 180);
		contentPane.add(table);
	}
	
	
	public class TmMusica extends AbstractTableModel {	

		private	String[] columnNames = { "Nombre", "Cantante", "Discografica", "Duracion", "Precio" };
		
		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		
		/*
		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
			
			
			super.setValueAt(aValue, rowIndex, columnIndex);
		}
*/


		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		@Override
		public int getRowCount() {
			return cervezas.size();
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			if (col < 2) {
				return false;
			} else {
				return true;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			ServidorServicioMusicaProxy proxy = new ServidorServicioMusicaProxy();
			Musica musica = proxy.obtenerMusicas()[rowIndex];
			String info=null;
			
			switch (columnIndex) {
			case 0:
				info = musica.getNombre();
				break;
			case 1:
				info = String.valueOf(musica.getCantante());
				break;
			case 2:
				info = String.valueOf(musica.getCantidad());
				break;
			case 3:
				info = musica.getOpinion();
				break;
			case 4:
				info = musica.getDescripcion();
				break;
			case 5:
				info = musica.getIngredientes();
				break;	
			case 6: 
				info = String.valueOf(musica.getId());
				break;
			default:
				return info;
			}
			return info;
			
		}

	
	}
	
	
}


