/**
 * ServicioMusicaServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public class ServicioMusicaServiceLocator extends org.apache.axis.client.Service implements servicios.ServicioMusicaService {

    public ServicioMusicaServiceLocator() {
    }


    public ServicioMusicaServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServicioMusicaServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ServicioMusicaPort
    private java.lang.String ServicioMusicaPort_address = "http://localhost:8080/webServicesServidorCompras/services/ServicioMusicaPort";

    public java.lang.String getServicioMusicaPortAddress() {
        return ServicioMusicaPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ServicioMusicaPortWSDDServiceName = "ServicioMusicaPort";

    public java.lang.String getServicioMusicaPortWSDDServiceName() {
        return ServicioMusicaPortWSDDServiceName;
    }

    public void setServicioMusicaPortWSDDServiceName(java.lang.String name) {
        ServicioMusicaPortWSDDServiceName = name;
    }

    public servicios.ServidorServicioMusica getServicioMusicaPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ServicioMusicaPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getServicioMusicaPort(endpoint);
    }

    public servicios.ServidorServicioMusica getServicioMusicaPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servicios.ServicioMusicaServiceSoapBindingStub _stub = new servicios.ServicioMusicaServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getServicioMusicaPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setServicioMusicaPortEndpointAddress(java.lang.String address) {
        ServicioMusicaPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servicios.ServidorServicioMusica.class.isAssignableFrom(serviceEndpointInterface)) {
                servicios.ServicioMusicaServiceSoapBindingStub _stub = new servicios.ServicioMusicaServiceSoapBindingStub(new java.net.URL(ServicioMusicaPort_address), this);
                _stub.setPortName(getServicioMusicaPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ServicioMusicaPort".equals(inputPortName)) {
            return getServicioMusicaPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios/", "ServicioMusicaService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios/", "ServicioMusicaPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ServicioMusicaPort".equals(portName)) {
            setServicioMusicaPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
