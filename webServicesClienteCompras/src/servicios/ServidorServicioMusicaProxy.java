package servicios;

public class ServidorServicioMusicaProxy implements servicios.ServidorServicioMusica {
  private String _endpoint = null;
  private servicios.ServidorServicioMusica servidorServicioMusica = null;
  
  public ServidorServicioMusicaProxy() {
    _initServidorServicioMusicaProxy();
  }
  
  public ServidorServicioMusicaProxy(String endpoint) {
    _endpoint = endpoint;
    _initServidorServicioMusicaProxy();
  }
  
  private void _initServidorServicioMusicaProxy() {
    try {
      servidorServicioMusica = (new servicios.ServicioMusicaServiceLocator()).getServicioMusicaPort();
      if (servidorServicioMusica != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servidorServicioMusica)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servidorServicioMusica)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servidorServicioMusica != null)
      ((javax.xml.rpc.Stub)servidorServicioMusica)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.ServidorServicioMusica getServidorServicioMusica() {
    if (servidorServicioMusica == null)
      _initServidorServicioMusicaProxy();
    return servidorServicioMusica;
  }
  
  public void registrarMusica(servicios.Musica arg0) throws java.rmi.RemoteException{
    if (servidorServicioMusica == null)
      _initServidorServicioMusicaProxy();
    servidorServicioMusica.registrarMusica(arg0);
  }
  
  public servicios.Musica[] obtenerMusicas() throws java.rmi.RemoteException{
    if (servidorServicioMusica == null)
      _initServidorServicioMusicaProxy();
    return servidorServicioMusica.obtenerMusicas();
  }
  
  
}