package main;

import facade.GestorHipotecas;

public class Main {

	public static void main(String[] args) {
		int idCliente = 44;
		// EL patron FACADE no indica que de una gran multitud de subsisttemas para
		// UNA funcionalidad concreta como es concederHipoteca, no tengamos que buscar 
		// , entender y usar multiples subsistemas sino simplemente un elemento FACADE
		// en este caso gestor hipotecas que ya sabe cuales usar.
		GestorHipotecas gh = new GestorHipotecas();
		boolean resultaddo = gh.concederHipoteca(50000, idCliente);
		System.out.println("Resultado de conceder hipoteca " + resultaddo);
		
		

	}

}
