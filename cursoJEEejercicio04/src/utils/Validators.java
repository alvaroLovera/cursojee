package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

	public static boolean validatorNombre(String nombre) {

		String nameRule = "[a-z A-Z��������������]{3,50}";
		Pattern patron = Pattern.compile(nameRule);
		if (!nombre.isEmpty()) {
			Matcher match = patron.matcher(nombre);
			return match.matches();
		}
		return false;
	}

	public static boolean validatorApellidos(String apellidos) {
		String apellidosRule = "[a-z A-Z��������������]{3,20}";
		Pattern patron = Pattern.compile(apellidosRule);
		Matcher match = patron.matcher(apellidos);
		return match.matches();
	}

	public static boolean validatorEmail(String email) {
		String emailRule = "^[A-Za-z0-9+_.-]+@(.+)$";
		Pattern patron = Pattern.compile(emailRule);
		Matcher match = patron.matcher(email);
		return match.matches();
	}

	public static boolean validatorFecha(String fecha) {
		String fechaRule = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$";
		Pattern patron = Pattern.compile(fechaRule);
		Matcher match = patron.matcher(fecha);
		// return match.matches();
		return true;
	}

	public static boolean validatorNumero(String nPaginas) {
		String numberRule = "[0-9]+";
		Pattern patron = Pattern.compile(numberRule);
		Matcher match = patron.matcher(nPaginas);
		return match.matches();
	}

	public static boolean validatorPrecio(String precio) {
		String precioRule = "[0-9]+\\.[0-9]+";
		Pattern patron = Pattern.compile(precioRule);
		Matcher match = patron.matcher(precio);
		return match.matches();
	}

}
