package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LibroDAOImpl extends MasterDAO implements LibroDAO {

	@Override
	public int registrarLibro(Libro l) {
		conectar();
		int idRegistrado = -1;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERT_LIBRO,
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, l.getTitulo());
			ps.setString(2, l.getAutor());
			ps.setString(3, l.getGenero());
			ps.setString(4, l.getEditorial());
			ps.setInt(5, l.getnPaginas());
			ps.setDouble(6, l.getPrecio());
			ps.setInt(7, l.getIdCategoria());

			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				idRegistrado = rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return idRegistrado;
	}

	@Override
	public ArrayList<Libro> obtenerLibros() {
		conectar();
		ArrayList<Libro> libros = new ArrayList<Libro>();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_LIBROS);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Libro l = new Libro();
				l.setId(rs.getInt("id"));
				l.setTitulo(rs.getString("Titulo"));
				l.setAutor(rs.getString("autor"));
				// l.setGenero(rs.getString("Genero"));//Nombre categoria
				l.setEditorial(rs.getString("editorial"));
				// l.setnPaginas(rs.getInt("nPaginas"));
				// l.setPrecio(rs.getDouble("precio"));
				l.setPrecio(rs.getDouble("euros"));

				Categoria c = new Categoria();
				c.setNombre(rs.getString("Genero"));
				l.setCategoria(c);
				libros.add(l);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return libros;
	}

	@Override
	public void borrarLibro(String idLibroAborrar) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_BORRAR_LIBRO);
			ps.setString(1, idLibroAborrar);
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public Libro obtenerLibroPorId(String idLibroEditar) {
		conectar();
		Libro l = new Libro();

		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_LIBRO_POR_ID);
			ps.setString(1, idLibroEditar);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				l.setId(rs.getInt("id"));
				l.setTitulo(rs.getString("titulo"));
				l.setAutor(rs.getString("autor"));
				l.setEditorial(rs.getString("editorial"));
				l.setGenero(rs.getString("genero"));
				l.setnPaginas(rs.getInt("nPaginas"));
				l.setPrecio(rs.getDouble("precio"));
				l.setIdCategoria(rs.getInt("id_categoria"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return l;
	}

	@Override
	public void actualizarLibro(Libro libroGuardarCambios) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_ACTUALIZAR_LIBRO);
			ps.setString(1, libroGuardarCambios.getTitulo());
			ps.setString(2, libroGuardarCambios.getAutor());
			ps.setString(3, libroGuardarCambios.getGenero());
			ps.setString(4, libroGuardarCambios.getEditorial());
			ps.setInt(5, libroGuardarCambios.getnPaginas());
			ps.setDouble(6, libroGuardarCambios.getPrecio());
			ps.setInt(7, libroGuardarCambios.getIdCategoria());
			ps.setInt(8, libroGuardarCambios.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public ArrayList<Libro> obtenerLibrosPorNombreIndicandoComienzoYcuantos(String nombreAbuscar, int comienzo,
			int cuantos) {
		conectar();
		ArrayList<Libro> libros = new ArrayList<Libro>();
		try {
			PreparedStatement ps = conexion
					.prepareStatement(ConstantesSQL.SQL_OBTENER_LIBROS_INDICANDO_COMIENZA_CUANTOS);
			ps.setString(1, "%" + nombreAbuscar + "%");
			ps.setInt(2, comienzo);
			ps.setInt(3, cuantos);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Libro l = new Libro();
				l.setTitulo(rs.getString("titulo"));
				l.setAutor(rs.getString("autor"));
				l.setEditorial(rs.getString("editorial"));
				l.setGenero(rs.getString("genero"));
				l.setIdCategoria(rs.getInt("id_categoria"));
				l.setPrecio(rs.getDouble("precio"));
				l.setId(rs.getInt("id"));
				l.setnPaginas(rs.getInt("nPaginas"));
				libros.add(l);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return libros;
	}

	@Override
	public int obtenerTotalLibros(String tituloAbuscar) {

		conectar();
		int total = 0;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTEBER_TOTAL_LIBROS);
			ps.setString(1, "%" + tituloAbuscar + "%");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				total = rs.getInt("total");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();

		return total;
	}

	@Override
	public Libro obtenerLibroPorIdEnCarrito(String idLibroEditar) {
		// TODO Auto-generated method stub
		return null;
	}

}
