package modelo;

import java.util.List;

public interface CategoriaDAO {

	int registrarCategoria(Categoria nueva);

	List<Categoria> obtenerCategorias();

	void borrarCategoria(int idInt);

	Categoria obtenerCategoriaPorId(int idCategoriaEditar);

	void actualizarCategoria(Categoria editar);

}
