package modelo;

import java.util.ArrayList;

public interface LibroDAO {
	
	public int registrarLibro(Libro l);
	public ArrayList<Libro> obtenerLibros();
	public void borrarLibro(String idLibroAborrar);
	public Libro obtenerLibroPorId(String idLibroEditar);
	public void actualizarLibro(Libro libroGiuardarCambios);
	public ArrayList<Libro> obtenerLibrosPorNombreIndicandoComienzoYcuantos(String nombreAbuscar, int comienzo,
			int cuantos);
	public int obtenerTotalLibros(String nombreAbuscar);
	Libro obtenerLibroPorIdEnCarrito(String idLibroEditar);
}
