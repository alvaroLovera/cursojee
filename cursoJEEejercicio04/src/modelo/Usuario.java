package modelo;

import java.util.Date;
import java.util.List;

public class Usuario {
	
	private String name, apellidos, email, pass, genero;
	private int id;
	private String fechaNacimiento;
	private List<Libro> librosCarrito;
	
	public Usuario() {
	}
	
	public Usuario(String name, String apellidos, String email, String pass, String genero, String fechaNacimiento) {
		this.name = name;
		this.apellidos = apellidos;
		this.email = email;
		this.pass = pass;
		this.genero = genero;
		this.fechaNacimiento = fechaNacimiento;
	}
	
	

	public Usuario(String name, String apellidos, String email, String pass, String genero, int id,
			String fechaNacimiento) {
		this.name = name;
		this.apellidos = apellidos;
		this.email = email;
		this.pass = pass;
		this.genero = genero;
		this.id = id;
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	
	public List<Libro> getLibrosCarrito() {
		return librosCarrito;
	}

	public void setLibrosCarrito(List<Libro> librosCarrito) {
		this.librosCarrito = librosCarrito;
	}

	@Override
	public String toString() {
		return "Usuario [name=" + name + ", apellidos=" + apellidos + ", email=" + email + ", pass=" + pass
				+ ", genero=" + genero + ", id=" + id + ", fechaNacimiento=" + fechaNacimiento + "]";
	}
	
	
	
}
