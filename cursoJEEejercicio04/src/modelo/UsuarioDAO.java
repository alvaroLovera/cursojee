package modelo;

import java.util.ArrayList;
import java.util.List;

public interface UsuarioDAO {
	
	public int registrarUsuario(Usuario u);
	public int getIdUsuario(String email, String pass);
	public ArrayList<Usuario> obtenerUsuarios();
	public void borrarUsuario(String idUsuarioAborrar);
	public Usuario obtenerUsuarioPorId(int idInt);
	public void actualizarUsuario(Usuario guardarCambios);
	public List<Usuario> obtenerUsuariosPorNombre(String nombreAbuscar);
	public List<Usuario> obtenerUsuariosPorNombreIndicandoComienzoYcuantos(String nombreAbuscar, int comienzo,
			int cuantos);
	public int obtenerTotalUsuarios(String nombreAbuscar);
}
