package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAOImpl extends MasterDAO implements UsuarioDAO {

	@Override
	public int registrarUsuario(Usuario u) {
		conectar();
		int idGenerada = -1;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERT_USUARIO,
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, u.getName());
			ps.setString(2, u.getApellidos());
			ps.setString(3, u.getEmail());
			ps.setString(4, u.getPass());
			ps.setString(5, u.getGenero());
			ps.setString(6, u.getFechaNacimiento());
			ps.execute();
			// De la siguiente obtener el id generado
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				idGenerada = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return idGenerada;
	}

	@Override
	public int getIdUsuario(String email, String pass) {

		conectar();
		int idUsuario = -1;
		try {
			PreparedStatement pr = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_ID_USUARIO);
			pr.setString(1, email);
			pr.setString(2, pass);
			ResultSet rs = pr.executeQuery();

			if (rs.next()) {
				idUsuario = rs.getInt("id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		desconectar();
		return idUsuario;
	}

	@Override
	public ArrayList<Usuario> obtenerUsuarios() {
		conectar();
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_USUARIOS);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Usuario u = new Usuario();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("nombre"));
				u.setEmail(rs.getString("email"));
				u.setApellidos(rs.getString("apellidos"));
				u.setFechaNacimiento(rs.getString("fechaNacimiento"));
				u.setGenero(rs.getString("genero"));
				u.setPass(rs.getString("pass"));
				usuarios.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return usuarios;
	}

	@Override
	public void borrarUsuario(String idUsuarioAborrar) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_BORRAR_USUARIO);
			ps.setString(1, idUsuarioAborrar);
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public Usuario obtenerUsuarioPorId(int idInt) {
		conectar();
		Usuario u = new Usuario();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_USUARIO_POR_ID);
			ps.setInt(1, idInt);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// Next situa la result set en el primer resultado
				// Que aun no he procesado
				u.setName(rs.getString("nombre"));
				u.setApellidos(rs.getString("apellidos"));
				u.setEmail(rs.getString("email"));
				u.setFechaNacimiento(rs.getString("fechaNacimiento"));
				u.setId(rs.getInt("id"));
				u.setPass(rs.getString("pass"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return u;
	}

	@Override
	public void actualizarUsuario(Usuario guardarCambios) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_ACTUALIZAR_USUARIO);
			ps.setString(1, guardarCambios.getName());
			ps.setString(2, guardarCambios.getApellidos());
			ps.setString(3, guardarCambios.getEmail());
			ps.setString(4, guardarCambios.getPass());
			ps.setString(5, guardarCambios.getGenero());
			ps.setString(6, guardarCambios.getFechaNacimiento());
			ps.setInt(7, guardarCambios.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public List<Usuario> obtenerUsuariosPorNombre(String nombreAbuscar) {
		conectar();
		List<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_USUARIOS_POR_NOMBRE);
			ps.setString(1, "%" + nombreAbuscar + "%");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Usuario u = new Usuario();
				u.setName(rs.getString("nombre"));

				usuarios.add(u);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();

		return usuarios;
	}

	@Override
	public List<Usuario> obtenerUsuariosPorNombreIndicandoComienzoYcuantos(String nombreAbuscar, int comienzo,
			int cuantos) {
		conectar();
		List<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			PreparedStatement ps = conexion
					.prepareStatement(ConstantesSQL.SQL_OBTENER_USUARIOS_INDICANDO_COMIENZA_CUANTOS);
			ps.setString(1, "%" + nombreAbuscar + "%");
			ps.setInt(2, comienzo);
			ps.setInt(3, cuantos);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Usuario u = new Usuario();
				u.setName(rs.getString("nombre"));
				u.setApellidos(rs.getString("apellidos"));
				u.setEmail(rs.getString("email"));
				u.setGenero(rs.getString("genero"));
				u.setFechaNacimiento(rs.getString("fechaNacimiento"));
				u.setId(rs.getInt("id"));
				u.setPass(rs.getString("pass"));
				usuarios.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuarios;
	}

	@Override
	public int obtenerTotalUsuarios(String nombreAbuscar) {
		conectar();
		int total = 0;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTEBER_TOTAL_USUARIOS);
			ps.setString(1, "%"+nombreAbuscar+"%");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				total = rs.getInt("total");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return total;
	}

}
