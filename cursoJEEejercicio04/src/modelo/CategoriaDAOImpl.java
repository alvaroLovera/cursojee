package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAOImpl extends MasterDAO implements CategoriaDAO {

	@Override
	public int registrarCategoria(Categoria nueva) {
		conectar();
		int idGenerada = -1;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERT_CATEGORIA, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, nueva.getNombre());
			ps.setString(2, nueva.getDescripcion());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()) {
				idGenerada = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return idGenerada;
	}

	@Override
	public List<Categoria> obtenerCategorias() {
		conectar();
		List<Categoria> categorias = new ArrayList<Categoria>();
		Categoria categoria = null;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_CATEGORIAS);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				categoria = new Categoria();
				categoria.setId(rs.getInt("id"));
				categoria.setNombre(rs.getString("nombre"));
				categoria.setDescripcion(rs.getString("descripcion"));
				categorias.add(categoria);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return categorias;
	}

	@Override
	public void borrarCategoria(int idInt) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_BORRAR_CATEGORIA);
			ps.setInt(1, idInt);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public Categoria obtenerCategoriaPorId(int idCategoriaEditar) {
		conectar();
		Categoria c = null;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_CATEGORIA_POR_ID);
			ps.setInt(1, idCategoriaEditar);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				c = new Categoria();
				c.setId(rs.getInt("id"));
				c.setNombre(rs.getString("nombre"));
				c.setDescripcion(rs.getString("descripcion"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return c;
	}

	@Override
	public void actualizarCategoria(Categoria editar) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_ACTUALIZAR_CATEGORIA);
			ps.setString(1, editar.getNombre());
			ps.setString(2, editar.getDescripcion());
			ps.setInt(3, editar.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

}
