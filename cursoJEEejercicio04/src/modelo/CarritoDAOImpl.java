package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarritoDAOImpl extends MasterDAO implements CarritoDAO {

	
	public CarritoDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public void guardarEnCarritoCarrito(Carrito c) {

		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_GUARDAR_EN_CARRITO);
			ps.setInt(1, c.getCantidad());
			ps.setInt(2, c.getIdUsuario());
			ps.setInt(3, 0);
			ps.setInt(4, c.getIdProducto());
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public List<Libro> obtenerLibrosEnCarritoPorUsuarioId(int idUsuario) {
		conectar();
		List<Libro> librosEnCarrito = new ArrayList<Libro>();
		LibroDAO libroDAO = new LibroDAOImpl();
		Libro l = null;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_LIBROS_DE_CARRITO_POR_USUARIO_ID);
			ps.setInt(1, idUsuario);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				l = new Libro();
				String idLibro = String.valueOf(rs.getInt(1));
				l = libroDAO.obtenerLibroPorIdEnCarrito(idLibro);
				librosEnCarrito.add(l);
			}		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return librosEnCarrito;
	}

	@Override
	public Libro obtenerLibroPorIdEnCarrito(String idLibroEditar) {
		//conectar();
		Libro l = new Libro();

		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_LIBRO_POR_ID);
			ps.setString(1, idLibroEditar);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				l.setId(rs.getInt("id"));
				l.setTitulo(rs.getString("titulo"));
				l.setAutor(rs.getString("autor"));
				l.setEditorial(rs.getString("editorial"));
				l.setGenero(rs.getString("genero"));
				l.setnPaginas(rs.getInt("nPaginas"));
				l.setPrecio(rs.getDouble("precio"));
				l.setIdCategoria(rs.getInt("id_categoria"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//desconectar();
		return l;
	}
	
}
