package modelo;

public class ConstantesSQL {
	
	public final static String SQL_INSERT_USUARIO = "INSERT INTO tabla_usuarios"
			+ "(nombre,apellidos, email,pass,genero,fechaNacimiento) value(?,?,?,?,?,?)";
	
	public final static String SQL_OBTENER_ID_USUARIO = "select id from tabla_usuarios where email = ? "
			+ "AND pass = ?";
	
	public static final String SQL_OBTENER_USUARIOS = "select * from tabla_usuarios";
	
	public static final String SQL_INSERT_LIBRO =  "INSERT INTO tabla_libros"
			+ "(titulo, autor, genero, editorial, nPaginas, precio, id_categoria) value(?,?,?,?,?,?,?)";
	
	public static final String SQL_OBTENER_LIBROS = 
			"SELECT tl.id, tl.titulo, tl.autor,  tl.editorial,tl.precio , tc.nombre, tl.titulo as Titulo, tc.nombre as Genero, tl.precio as euros FROM tabla_libros as tl, tabla_categorias as tc where tl.id_categoria = tc.id;"; 
		//	"SELECT tl.titulo, tl.precio , tc.nombre, tl.titulo as Titulo, tc.nombre as Genero, tl.precio as euros FROM tabla_libros as tl, tabla_categorias as tc where tl.id_categoria = tc.id;"; 
					 //"select * from tabla_libros";

	public static final String SQL_BORRAR_USUARIO = "DELETE FROM tabla_usuarios WHERE id = ?";

	public static final String SQL_BORRAR_LIBRO =  "DELETE FROM tabla_libros WHERE id = ?";

	public static final String SQL_OBTENER_LIBRO_POR_ID = "select * from tabla_libros WHERE id = ?";

	public static final String SQL_OBTENER_USUARIO_POR_ID = "select * from tabla_usuarios WHERE id = ?";

	public static final String SQL_ACTUALIZAR_USUARIO = "UPDATE tabla_usuarios SET nombre = ?, apellidos = ?, email = ?,"
			+ " pass = ?, genero = ?, fechaNacimiento = ? where id = ?";

	public static final String SQL_ACTUALIZAR_LIBRO = "UPDATE tabla_libros SET titulo = ?, autor = ?, genero = ?,"
			+ " editorial = ?, nPaginas = ?, precio = ?, id_categoria = ? where id = ?";

	public static final String SQL_INSERT_CATEGORIA = "INSERT INTO tabla_categorias"
			+ "(nombre, descripcion) value(?,?);";

	public static final String SQL_OBTENER_CATEGORIAS = "Select * from tabla_categorias;";

	public static final String SQL_BORRAR_CATEGORIA = "DELETE FROM tabla_categorias WHERE id = ?;";

	public static final String SQL_OBTENER_CATEGORIA_POR_ID = "select * from tabla_categorias WHERE id = ?";

	public static final String SQL_ACTUALIZAR_CATEGORIA = "UPDATE tabla_categorias SET nombre = ?, descripcion = ? where id = ?;";

	public static final String SQL_OBTENER_USUARIOS_POR_NOMBRE = "select * from tabla_usuarios WHERE nombre like ? ;";

	public static final String SQL_OBTENER_USUARIOS_INDICANDO_COMIENZA_CUANTOS = "select * from tabla_usuarios WHERE nombre like ? order by id asc limit ? , ? ;";

	public static final String SQL_OBTEBER_TOTAL_USUARIOS = "select count(id) as total from tabla_usuarios WHERE nombre like ? ;";

	public static final String SQL_OBTEBER_TOTAL_LIBROS = "select count(id) as total from tabla_libros WHERE titulo like ? ;";

	public static final String SQL_OBTENER_LIBROS_INDICANDO_COMIENZA_CUANTOS = "select * from tabla_libros WHERE titulo like ? order by id asc limit ? , ? ;";

	public static final String SQL_GUARDAR_EN_CARRITO = "INSERT INTO tabla_carrito"
			+ "(cantidad, id_usuario, id_pedido, id_libro) value(?,?,?,?)";

	public static final String SQL_OBTENER_LIBROS_DE_CARRITO_POR_USUARIO_ID = "SELECT ca.id_libro " + 
			"FROM tabla_usuarios as us, tabla_carrito as ca " + 
			"where us.id = ? AND ca.id_pedido = 0;";
	
}
