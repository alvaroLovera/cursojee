package servlet.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;

/**
 * Servlet implementation class ServletAdminListarLibros
 */
@WebServlet("/admin/ServletAdminListarLibros")
public class ServletAdminListarLibros extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String campoBusqueda = request.getParameter("campoBusqueda");
		String tituloAbuscar = "";
		if(campoBusqueda != null) {
			tituloAbuscar = campoBusqueda;
		}
		// Paginaciones
		int comienzo = 0;
		int cuantos = 10;
		if(request.getParameter("comienzo") != null) {
			comienzo = Integer.parseInt(request.getParameter("comienzo"));
		}
		int siguiente = comienzo + cuantos;
		int anterior = comienzo - cuantos;
		request.setAttribute("siguiente", siguiente);
		request.setAttribute("anterior", anterior);
			
		LibroDAO librosoDAO = new LibroDAOImpl();
		ArrayList<Libro> libros = librosoDAO.obtenerLibrosPorNombreIndicandoComienzoYcuantos(tituloAbuscar,comienzo,cuantos);//obtenerLibros();
		int totalResultados = librosoDAO.obtenerTotalLibros(tituloAbuscar);
		request.setAttribute("libros", libros);
		request.setAttribute("totalResultados", totalResultados);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/libros.jsp");
		rd.forward(request, response);
	}

}
