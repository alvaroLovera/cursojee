package servlet.admin;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminRegistrarCategoria
 */
@MultipartConfig
@WebServlet("/admin/ServletAdminRegistrarCategoria")
public class ServletAdminRegistrarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("campoNombre");
		String descripcion = request.getParameter("campoDescripcion");
		
		String ruta = "";
		
		String errorNombre = "";
		String errorDescripcion = "";
		
		if(!Validators.validatorNombre(nombre)) errorNombre = "Nombre no valido";
		if(descripcion.isEmpty()) errorDescripcion = "Rellene la descripción";
		
		if(errorDescripcion.isEmpty() || errorNombre.isEmpty()) {
			
			Categoria nueva = new Categoria(nombre,descripcion);
			CategoriaDAO categoriaDAO = new CategoriaDAOImpl();
				
			int idGenerada = categoriaDAO.registrarCategoria(nueva);
			System.out.println("Id generada: " + idGenerada);
			// vamos a guardar la foto subida en la carpeta imagenes
			String carpetaSubidas = getServletContext().getRealPath("") + File.separator + "categorias";
			File uploadDir = new File(carpetaSubidas);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
				System.out.println("Creada la carpeta: " + uploadDir);
			}
			System.out.println("Guardado archivo subido en: " + uploadDir);
			Part imagenSubida =	request.getPart("campoImagen");//guardado la foto subida de campo imagen
			imagenSubida.write(carpetaSubidas + File.separator + idGenerada + ".jpg");
			
			ruta = "/admin/ServletAdminListarCategorias";
		}else {
			ruta = "/admin/categoriaForm.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

}
