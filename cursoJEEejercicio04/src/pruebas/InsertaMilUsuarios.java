package pruebas;

import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;

public class InsertaMilUsuarios {
	public static void main(String[] args) {
		String nombre = "Eva";
		
		UsuarioDAO uDAO = new UsuarioDAOImpl();
		Usuario u = new Usuario();
		u.setName(nombre);
		u.setEmail("eva@mail.com");
		u.setGenero("Mujer");
		u.setPass("123");
		u.setApellidos("");
		u.setFechaNacimiento("01/01/1991");
		
		for (int i = 0; i < 1000; i++) {
			System.out.println(i);
			u.setName(nombre+i);
			u.setPass(u.getPass());
			u.setEmail(u.getEmail());
			u.setFechaNacimiento(u.getFechaNacimiento());
			uDAO.registrarUsuario(u);
		}
		System.out.println("Registrados 1000 usuarios");
	}
}
