package filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Filtro para proteger la ruta /admin
public class FiltroAdmin implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest requestPrimitivo, ServletResponse responsePrimitivo, FilterChain chain)
			throws IOException, ServletException {
		// COdigo que se va a ejecutar antes de lo que se ejecute 
		// en la ruta indicada para el filtro que sera /adminw
		
		// En un filtro nos dan un request y un responde que no son 
		// los mismors . Pero haciendo un casting podemos tener
		// el request y el response con los que hemos trabajado en los servlets
		HttpServletRequest request = (HttpServletRequest) requestPrimitivo;
		HttpServletResponse response = (HttpServletResponse) responsePrimitivo;
		
		// este filtro mmira si se ha mandado ua pass correcta por el filtro de admin y si la pass es correcto meto en sesssion un elemento 
		//Llmadao admin que valga ok
		if(request.getParameter("pass") != null && request.getParameter("pass").equals("123")){
			request.getSession().setAttribute("admin", "ok");
		}
		// En este ejemplo vamos a entender que un  admin es admin si tiene en session un elemento al valor OK 
		if(request.getSession().getAttribute("admin") !=null && request.getSession().getAttribute("admin").equals("ok")) {
			// Si se cumple este if permito que la sesion siga adelante eso debe ir en el web.xml
			chain.doFilter(request, response);
			
		}else {
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/admin/login.jsp");
			rd.forward(request, response);		
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	

}
