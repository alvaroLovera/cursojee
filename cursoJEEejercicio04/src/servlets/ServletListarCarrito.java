package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.CarritoDAO;
import modelo.CarritoDAOImpl;
import modelo.Libro;

/**
 * Servlet implementation class ServletListarCarrito
 */
@WebServlet("/ServletListarCarrito")
public class ServletListarCarrito extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idUsuario = (int) request.getSession().getAttribute("id");
		CarritoDAO carritoDAO = new CarritoDAOImpl();
		 List<Libro> librosCarrito = carritoDAO.obtenerLibrosEnCarritoPorUsuarioId(idUsuario);
		 
		request.setAttribute("librosCarrito", librosCarrito);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/zonaPrivada.jsp");
		rd.forward(request, response);
	}

}
