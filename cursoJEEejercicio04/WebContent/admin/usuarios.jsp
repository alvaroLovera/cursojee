<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Listado de Usuarios Registrados</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	Listado de usuarios registrados:
	<br />
	
	<form action="ServletAdminListarUsuarios" >
		Buscar:<input type="text" name="campoBusqueda"/>
		<input type="submit" value="Buscar" /> 
	</form> 
	<div>
		<c:if test="${anterior >= 0}">
			<a href="ServletAdminListarUsuarios?comienzo=${anterior}">Anterior</a>
		</c:if>
		Total Resultados : ${totalResultados}
		<c:if test="${siguiente < totalResultados}">	
			<a href="ServletAdminListarUsuarios?comienzo=${siguiente}">Siguiente</a>
		</c:if>
	</div>
	
	
	<table class="table" style="width: 100%">
		<thead class="thead-dark">
			<tr>
				<th>Avatar</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>ID</th>				
				<th>EDITAR</th>
				<th>BORRAR</th>
			</tr>
			<c:forEach var="usuario" items="${usuarios}">
			<tr>
				<td><img height="120px" src="../usuarios/${usuario.id}.jpg?p=<%= new Date().toString() %>" /></td>
				<td>${usuario.name}</td>
				<td>${usuario.email}</td>
				<td>${usuario.id}</td>
				<td><a onclick="return confirmacion();"
					href="ServletAdminBorrarUsuario?id=${usuario.id}">BORRAR</a></td>
				<td><a href="ServletAdminEditarUsuario?id=${usuario.id}">EDITAR</a></td>
			</tr>
		</c:forEach>
		</thead>
	</table>


	<script type="text/javascript">
		function confirmacion() {
			var resultado = confirm("�Est� seguro?");
			return resultado;
		}
	</script>



</body>
</html>