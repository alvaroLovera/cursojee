<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/bootstrap.min.js"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	   
		
<div class="navbar" id="navbarSupportedContent">
	<ul class="navbar-nav mr-auto">	
	     <li class="nav-item active">    
			<a class="nav-link" href="index.jsp">INICIO</a>
	     </li>
		<li class="nav-item">
			<a class="nav-link"href="ServletAdminListarUsuarios">Gestionar Usuarios</a>
		</li>
		<li class="nav-item" >
			<a class="nav-link" href="ServletAdminListarLibros">Gestionar Libros</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="ServletAdminListarCategorias">Gestionar Categorias</a>
					
		</li>
		<li class="nav-item" >
<!-- 		<a class="nav-link" href="libroForm.jsp">Registrar Libros</a> -->
			<a class="nav-link" href="ServletAdminPrepararRegistroLibro">Registrar Libros</a> 
		</li>
		<li class="nav-item" >
			<a class="nav-link" href="categoriaForm.jsp">Registrar Categorias</a>
		</li>
	</ul>
</div>
</nav>