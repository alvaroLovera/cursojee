<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Gesti&oacute;n de Libros</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<form action="ServletAdminGuardarCambiosLibros"  enctype="multipart/form-data"  method="post">
		<label>T�tulo: </label> <input type="text" name="campoTitulo"
			value="${libroAeditar.titulo}" /><span style="color: red;">${errorTitulo}</span><br />
		<label>Autor: </label> <input type="text" name="campoAutor"
			value="${libroAeditar.autor}" /><span style="color: red;">${errorAutor}</span><br />
<!-- 		<label>Genero: </label> <input type="text" name="campoGenero" -->
<%-- 			value="${libroAeditar.genero}" /><span style="color: red;">${errorGenero}</span><br /> --%>

		<label>Genero: </label>
			<select name="campoCategoria">
				<c:forEach items="${categorias}" var="c">
				  		<option 
				  		<c:if test="${libroAeditar.idCategoria == c.id}">selected</c:if>				  		
				  		 value="${c.id}">${c.nombre}</option>
				</c:forEach>
			</select>
		<label>Editorial: </label> <input type="text" name="campoEditorial"
			value="${libroAeditar.editorial}" /><span style="color: red;">${errorEditorial}</span><br />
		<label>Numero de paginas: </label> <input type="text"
			name="campoNpaginas" value="${libroAeditar.nPaginas}" /><span
			style="color: red;">${errorNpaginas}</span><br /> <label>Precio:
		</label> <input type="text" name="campoPrecio" value="${libroAeditar.precio}" /><span
			style="color: red;">${errorPrecio}</span><br />
			<img height="100px" src="../libros/${libroAeditar.id}.jpg?p=<%= new Date().toString() %>" />
			<span>Nueva Imagen</span>
			<input type="file" name="campoImagen" value="../libros/${libroAeditar.id}.jpg" size="1" /><br/>
			
			<input type="hidden" name="campoId" value="${libroAeditar.id}" /> 
			<input type="submit" value="Guardar Cambios">
	</form>
</body>
</html>