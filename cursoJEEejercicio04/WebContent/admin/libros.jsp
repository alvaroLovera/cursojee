<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	
	<form action="ServletAdminListarLibros" >
		Buscar:<input type="text" name="campoBusqueda"/>
		<input type="submit" value="Buscar" /> 
	</form> 
	<div>
		<c:if test="${anterior >= 0}">
			<a href="ServletAdminListarLibros?comienzo=${anterior}">Anterior</a>
		</c:if>
		Total Resultados : ${totalResultados}
		<c:if test="${siguiente < totalResultados}">	
			<a href="ServletAdminListarLibros?comienzo=${siguiente}">Siguiente</a>
		</c:if>
	</div>
	
	<table class="table" style="width: 100%">
		<thead class="thead-dark">
			<tr>
	<!-- 			<th>ID</th> -->
				<th>Portada</th>
				<th>Titulo</th>
				<th>Autor</th>
				<th>Genero</th>
				<th>Editorial</th>
				<th>Precio</th>
				<th>EDITAR</th>
				<th>BORRAR</th>
			</tr>
		</thead>
		<c:forEach var="libro" items="${libros}">
			<tr>
				<%-- 				<td>${libro.id}</td> --%>
				<%-- 				<td><img src="../libros/${libro.id}.jpg" /></td> --%>
				<%-- 				<td>${libro.titulo}</td> --%>
				<%-- 				<td>${libro.autor}</td> --%>
				<%-- 				<td>${libro.editorial}</td> --%>
				<%-- 				<td><a href="ServletAdminEditarLibro?id=${libro.id}">EDITAR</a></td> --%>
				<!-- 				<td><a onclick="return confirmacion()" -->
				<%-- 					href="ServletAdminBorrarLibro?id=${libro.id}">BORRAR</a></td> --%>
<%-- 				<td>${libro.id}</td> --%>
				<td><img height="100px" src="../libros/${libro.id}.jpg?p=<%= new Date().toString() %>" /></td>
				<td>${libro.titulo}</td>
				<td>${libro.autor}</td>
				<td>${libro.categoria.nombre}</td>
				<td>${libro.editorial}</td>
				<td>${libro.precio}</td>		
				<td><a href="ServletAdminEditarLibro?id=${libro.id}">EDITAR</a></td>
				<td><a onclick="return confirmacion()"
					href="ServletAdminBorrarLibro?id=${libro.id}">BORRAR</a></td>
			</tr>
		</c:forEach>
	</table>
	<script type="text/javascript">
		function confirmacion() {
			var resultado = confirm("�Est� seguro?");
			return resultado;
		}
	</script>
</body>
</html>