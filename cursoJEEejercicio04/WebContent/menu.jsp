<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="navbar" id="navbarSupportedContent">
	<ul class="navbar-nav mr-auto">	
	     <li class="nav-item active">    
			<a class="nav-link" href="index.jsp">INICIO</a>
	     </li>
		<li class="nav-item">
			<a class="nav-link"href="ServletAdminListarUsuarios">CATALOGO</a>
		</li>
		<li class="nav-item">
			<a class="nav-link"href="ofertas.jsp">OFERTAS</a>
		</li>
		<li class="nav-item">
			<a class="nav-link"href="registro.jsp">REGISTRO</a>
		</li>
		<li class="nav-item">
			<a class="nav-link"href="login.jsp">Login</a>
		</li>
		<li class="nav-item">
			<a class="nav-link"href="ServletListarCarrito">Carrito</a>
		</li>
	</ul>
</div>
</nav>