package modelo;

import java.util.List;

import servicios.Apuesta;

public interface ApuestasDAO {

	boolean registrarApuesta(Apuesta a);

	List<Apuesta> obtenerApuesta();

	void marcarApuestaComoGaanadora(int numero);

}