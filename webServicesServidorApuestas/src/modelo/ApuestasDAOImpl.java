package modelo;

import java.util.ArrayList;
import java.util.List;

import servicios.Apuesta;

public class ApuestasDAOImpl implements ApuestasDAO {

	private static List<Apuesta> apuestas = new ArrayList<Apuesta>();
	
	public boolean registrarApuesta(Apuesta a) {
		// Poner to Stringo no ponerlo al concatenar con texto es lo mismo
		System.out.println("Guardando en BD: " + a.toString());
		apuestas.add(a);
		return true;
	}
	
	public List<Apuesta> obtenerApuesta(){
		return apuestas;
	}
	
	public void marcarApuestaComoGaanadora(int numero) {
		for (Apuesta apuesta : apuestas) {
			if(apuesta.getNumero() == numero) {
				apuesta.setGanado(true);
			}
		}
	}
	
}
