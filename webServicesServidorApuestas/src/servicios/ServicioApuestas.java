package servicios;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.jws.WebService;

import modelo.ApuestasDAO;
import modelo.ApuestasDAOImpl;

public class ServicioApuestas {

	private ApuestasDAO apuestasDAO = new ApuestasDAOImpl();
	private Date datePrimeraApuesta = null;
	private int numeroGanador = -1;

	public boolean registrarApuesta(Apuesta a) {
		System.out.println("Recibido en el server: " + a);
		apuestasDAO.registrarApuesta(a);
		if (datePrimeraApuesta == null) {
			datePrimeraApuesta = new Date();

		}

		return true;
	}

	public int resolverApuestas() {
		if (numeroGanador == -1) {
			Date ahora = new Date();
			if ((ahora.getTime() - datePrimeraApuesta.getTime()) > 30000) {

				Random r = new Random();
				numeroGanador = r.nextInt((36) + 1);
				apuestasDAO.marcarApuestaComoGaanadora(numeroGanador);
			}
		}
		return numeroGanador;
	}

	public List<Apuesta> obtenerApuestas() {
		return apuestasDAO.obtenerApuesta();
	}

}
