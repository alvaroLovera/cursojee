package main;

public class Main {

	public static void main(String[] args) {
		
		
		// u es un objeto de la clase usuario
		Usuario u = new Usuario("Ana", "ana@mail.com");
		
		// pero realmente u es un referencia
		Usuario u2 = u;
		// La realidad es que u2, que es una referencia que apunta al contenido al
		// contenido de la referencia u de esta forma decimos que de forma coloqueal que u2
		// es u , o que u2 es lo mismo que u
		
		u2.setNombre("eva");
		System.out.println("nombre del objeto u: " + u.getNombre());
				
	}

}
