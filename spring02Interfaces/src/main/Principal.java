package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.PeliculasDAO;

public class Principal {

	public static void main(String[] args) {
		
//		Para crar un contenedor de Spring con lo puesto en el aplicationContext.xml
		ApplicationContext contenedor = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		// Cuando se pida un DAO o cualquier clase que implemente una interfaz lo suyo
		// es pedirla por el interfaz:
		PeliculasDAO pDAO = contenedor.getBean(PeliculasDAO.class);
		pDAO.borrarPelicula(11);
		// Asi he pedido una Bean de tipo peliculasDAO que es una interfaz. De un interfaz no se puede crear un  objeto
		// Poe lo que el contenedor de Spring me devuelve la unica Bean que tiene como class PeliculasDAOImpl. Esto es asi porqeu
		// Spring y Java etienden que la unca Bean disponible de PeliculasDAO es la Bean de PeliculasDAOImpl
		
		// Resumiendo: Si yo pido a Spring indicandole un interfaz, Spring me 
		// devuelve la bean de la clase que implementa el interfaz
		
	}

}
