package main;

import java.rmi.RemoteException;

import servicios.InterfazServicioApuestasProxy;

public class Main {

	public static void main(String[] args) {
		// Para usar el servicio web generamos un proxy 
		InterfazServicioApuestasProxy proxy = new InterfazServicioApuestasProxy();
		try {
			proxy.registrarApuesta(55, 250);
			System.out.println("Web service consumido correctamente");
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("No puede consumir el web service");
		}

	}

}
