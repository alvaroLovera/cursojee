package beans;

import org.springframework.stereotype.Component;

@Component
public class GestorUsuarios {
	
	public void borrarUsuario(int idUsuario) {
		System.out.println("Borrando usuario: " + idUsuario);
	}
}
