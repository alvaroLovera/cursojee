package main;

// Esta es la clase TEMPLATE
public abstract class Estadio {

	public void comenzarJuego() {
		sacarJugadoresAlCampo();
		System.out.println("Esperando a que todo el mundo este preparado");
		arrancarJuego();
		System.out.println("Esperando a que juego termine");
		finalizarJuego();
	}
	
	public abstract void finalizarJuego();
	
	public abstract void arrancarJuego();
	
	public abstract void sacarJugadoresAlCampo();
	
	
}
