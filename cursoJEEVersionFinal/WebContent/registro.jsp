<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<h4>Registro</h4>

<button onclick="validarImagen()">validar</button>
<form action="ServLetRegistroUsuario" onsubmit="return validarImagen();"  enctype="multipart/form-data"  method="post" >
<label>Nombre: </label>
<input type="text" name="campoNombre" value="${nombre}" /><span style="color:red;">${errorNombre}</span><br/>
<label>Apellidos: </label>
<input type="text" name="campoApellidos" value="${apellidos}" /><span style="color:red;">${errorApellidos}</span><br/>
<label>Email: </label>
<input type="email" name="campoEmail" value="${email}" /><span style="color:red;">${errorEmail}</span><br/>
<label>Contraseņa: </label>
<input type="password" name="campoPass" value="${pass}" /><span style="color:red;">${errorPass}</span><br/>
<label>Genero: </label>
<!-- <input type="text" name="campoGenero" /><br/> -->
<select name="campoGenero" >
<option value="hombre">Hombre</option>
<option value="mujer">Mujer</option>
<option value="no binario">No binario</option>
</select>
<label>Fecha Nacimiento: </label>
<input type="date" name="campoFecha" value="${fechaNaciento}" /><span style="color:red;">${errorFechaNacimiento}</span><br/>
Foto:
<input type="file"  id="imagen" name="campoImagen" size="1" /><br/>
<input type="submit" >
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
function validarImagen() {
    var fileSize = $('#imagen')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    var siezeMegaByte = parseInt(siezekiloByte / 1024);
    console.log(fileSize);
    console.log(siezekiloByte);
    console.log(siezeMegaByte);
    if (siezeMegaByte > 5) {	// if (siezeMegaByte >  $('#imagen').attr('size')) {
        alert("Imagen muy grande");
        return false;
    }else{
    	alert("Imagen correcta");
    	return true;
    }
}
</script>
</body>
</html>