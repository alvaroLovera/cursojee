<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
${compra}
<div  style="display:flex;" class="container">
	<c:forEach items="${libros}" var="l" >
	  <div style="flex-wrap: wrap; max-width: 90px; margin: 8px;" class="row">   
	      <img style=" margin: 8px;" width="90px" height="100px" src="libros/${l.id}.jpg?p=<%= new Date().toString() %>" />
				<p><b>${l.titulo}</b></p><br/>
				<p style="margin-bottom: 1px">${l.autor}</p><br/>
				<p style="margin-bottom: 1px">${l.categoria.nombre}</p><br/>
				<p style="margin-bottom: 1px">${l.editorial}</p><br/>
				<P style="margin-bottom: 1px">${l.precio}</P><br/>
				<input type="hidden" name="campoProductoId" /> 
				<a href="ServletGuardarCarrito?idLibro=${l.id}" >Comprar</a>
	  </div>
	</c:forEach>
</div>

</body>
</html>
<%-- <c:forEach items="${libros}" var="l" > --%>
<!-- 	<div> -->
<%-- 		${l.titulo}${l.precio}<br/> --%>
<!-- 	</div> -->
<%-- </c:forEach> --%>

<%-- <jsp:include page="menu.jsp"/> --%>