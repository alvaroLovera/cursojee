<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<table class="table">
  		<thead class="thead-dark">
			<tr>
				<th>ID</th>
				<th>IMAGEN</th>
				<th>NOMBRE</th>
				<th>DESCRIPCION</th>
				<th>EDITAR</th>
				<th>BORRAR</th>
			</tr>
		</thead>
		<c:forEach var="categoria" items="${categorias}">
			<tr>
				<td>${categoria.id}</td>
				<td><img src="../categorias/${categoria.id}.jpg?p=<%= new Date().toString() %>" /></td>
				<td>${categoria.nombre}</td>
				<td>${categoria.descripcion}</td>
				<td><a href="ServletAdminEditarCategoria?id=${categoria.id}">EDITAR</a></td>
				<td><a onclick="return confirmacion()"
					href="ServletAdminBorrarCategoria?id=${categoria.id}">BORRAR</a></td>
			</tr>
		</c:forEach>
	</table>
	<script type="text/javascript">
		function confirmacion() {
			var resultado = confirm("�Est� seguro?");
			return resultado;
		}
	</script>

</body>
</html>