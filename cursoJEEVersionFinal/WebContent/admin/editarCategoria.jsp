<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<form action="ServletAdminGuardarCambiosCategorias" method="post">
		<label>Nombre: </label> <input type="text" name="campoNombre"
			value="${categoriaAeditar.nombre}" /><span style="color: red;">${errorNombre}</span><br />
		<label>Descripcion: </label> <input type="text" name="campoDescripcion"
			value="${categoriaAeditar.descripcion}" /><span style="color: red;">${errorDescripcion}</span><br />
		<input type="hidden" name="campoId" value="${categoriaAeditar.id}" /><input
			type="submit" value="Guardar Cambios">
	</form>
</body>
</html>