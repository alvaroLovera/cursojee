<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<form action="ServletAdminRegistrarLibros"  enctype="multipart/form-data"  method="post" >
		<div class="form-group">	
			
			
				<label for="campoTitulo" >T�tulo: </label> 
				<input type="text" class="form-control" placeholder="T�tulo" name="campoTitulo"
					value="${titulo}" /><span style="color: red;">${errorTitulo}</span>	
			
			
			
			<label>Autor: </label> 
			<input type="text"  class="form-control" placeholder="Autor" name="campoAutor"
				value="${autor}" /><span style="color: red;">${errorAutor}</span><br />
			 
			
<!-- 			<input type="text" class="form-control" placeholder="Genero" name="campoGenero" -->
<%-- 				value="${genero}" /><span style="color: red;">${errorGenero}</span><br /> --%>
			
			
			<label>Editorial: </label>
			 <input type="text" class="form-control" placeholder="Editorial" name="campoEditorial"
				value="${editorial}" /><span style="color: red;">${errorEditorial}</span><br />
			
			
			<label>Numero de paginas: </label> 		
			<input type="text"
				name="campoNpaginas"  class="form-control" placeholder="T�tulo" value="${nPaginas}" /><span style="color: red;">${errorNpaginas}</span><br />
			 		
			<label>Precio: </label>
			<input type="date"  class="form-control" placeholder="T�tulo" name="campoPrecio"
				value="${precio}" /><span style="color: red;">${errorPrecio}</span><br/>
			
			
			<label>Genero: </label>
				<select  class="form-control" name="campoCategoria">
					<c:forEach items="${categorias}" var="c">
				  		<option value="${c.id}">${c.nombre}</option>	
					</c:forEach>
				</select>

			<input type="file" name="campoImagen" size="1" /><br/>
			<input type="submit" value="REGISTRAR LIBRO">
		</div>
	</form>
</body>
</html>