<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<form action="ServLetAdminGuardarCambiosUsuario" method="post">
		<label>Nombre: </label> <input type="text" name="campoNombre"
			value="${usuarioAeditar.name}" /><span style="color: red;">${errorNombre}</span><br />
		<label>Apellidos: </label> <input type="text" name="campoApellidos"
			value="${usuarioAeditar.apellidos}" /><span style="color: red;">${errorApellidos}</span><br />
		<label>Email: </label> <input type="email" name="campoEmail"
			value="${usuarioAeditar.email}" /><span style="color: red;">${errorEmail}</span><br />
		<label>Contraseņa: </label> <input type="password" name="campoPass"
			value="${usuarioAeditar.pass}" /><span style="color: red;">${errorPass}</span><br />
		<label>Genero: </label> <input type="text" name="campoGenero" /><br />
		<label>Fecha Nacimiento: </label> <input type="date" name="campoFecha"
			value="${usuarioAeditar.fechaNacimiento}" /><span
			style="color: red;">${errorFechaNacimiento}</span><br /> 
			
			<img height="100px" src="../usuarios/${usuario.id}.jpg" />
			
			<input
			type="hidden" name="campoId" value="${usuarioAeditar.id}" /> <input
			type="submit" value="Guardar Cambios">
	</form>

</body>
</html>