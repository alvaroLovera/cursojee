<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"></jsp:include>
<p>Se ha logueado con �xito</p>
${mensaje}<br/>
${id}<br/>
${email}<br/>
<h4>Carrito</h4>
<table class="table" style="width: 100%">
		<thead class="thead-dark">
			<tr>
	<!-- 			<th>ID</th> -->
				<th>Portada</th>
				<th>Titulo</th>
				<th>Autor</th>
				<th>Genero</th>
				<th>Editorial</th>
				<th>Precio</th>			
				<th>Quitar</th>
			</tr>
		</thead>
		<c:forEach var="libro" items="${librosCarrito}">
			<tr>
				<td><img height="100px" src="../libros/${libro.id}.jpg?p=<%= new Date().toString() %>" /></td>
				<td>${libro.titulo}</td>
				<td>${libro.autor}</td>
				<td>${libro.categoria.nombre}</td>
				<td>${libro.editorial}</td>
				<td>${libro.precio}</td>		
				<td><a onclick="return confirmacion()"
					href="ServletQuitarDelCarrito?id=${libro.id}">Quitar del Carrito</a></td>
			</tr>
		</c:forEach>
	</table>



</body>
</html>