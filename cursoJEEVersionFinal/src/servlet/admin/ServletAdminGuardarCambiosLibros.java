package servlet.admin;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminGuardarCambiosLibros
 */
@MultipartConfig
@WebServlet("/admin/ServletAdminGuardarCambiosLibros")
public class ServletAdminGuardarCambiosLibros extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("campoId");
		String ruta = "";
		String titulo = request.getParameter("campoTitulo");
		String autor = request.getParameter("campoAutor");
		String genero = request.getParameter("campoGenero");
		String editorial = request.getParameter("campoEditorial");
		String nPaginas = request.getParameter("campoNpaginas");
		String precio = request.getParameter("campoPrecio");
		String categoria = request.getParameter("campoCategoria");
		
		String errorId = "";
		String errorTitulo = "";
		String errorAutor = "";
		String errorEditorial = "";
		String errorNpaginas = "";
		String errorPrecio = "";
		
		if(!Validators.validatorNumero(id)) errorId = "Error id";
		if(!Validators.validatorNombre(titulo) || titulo.isEmpty()) errorTitulo = "Error titulo";
		if(!Validators.validatorNombre(autor) || autor.isEmpty()) errorAutor = "Error Autor";
		if(!Validators.validatorNombre(editorial) || editorial.isEmpty()) errorEditorial = "Error Editorial";
		if(!Validators.validatorNumero(nPaginas) || nPaginas.isEmpty()) errorNpaginas = "Error Paginas";
		if(!Validators.validatorPrecio(precio) || precio.isEmpty()) errorPrecio = "Error Precio";
		
		if(errorId.isEmpty() || errorTitulo.isEmpty() || errorAutor.isEmpty() ||
			errorEditorial.isEmpty() || errorNpaginas.isEmpty() || errorPrecio.isEmpty()) {
			
			int nPaginasInt = Integer.parseInt(nPaginas);
			double precioDouble = Double.parseDouble(precio);
			int idInt = Integer.parseInt(id);
			int idCategoria = Integer.parseInt(categoria);
			
			Libro libroGuardarCambios = new Libro(titulo,autor,genero,editorial,nPaginasInt,idInt,precioDouble);
			libroGuardarCambios.setIdCategoria(idCategoria);
			LibroDAO lisbrosDAO = new LibroDAOImpl();	
			lisbrosDAO.actualizarLibro(libroGuardarCambios);
			
			
			String carpetaSubidas = getServletContext().getRealPath("") + File.separator + "libros";
			File uploadDir = new File(carpetaSubidas);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
				System.out.println("Creada la carpeta: " + uploadDir);
			}
			System.out.println("Guardado archivo subido en: " + uploadDir);
			Part imagenSubida =	request.getPart("campoImagen");//guardado la foto subida de campo imagen
			imagenSubida.write(carpetaSubidas + File.separator + idInt + ".jpg");
			
			
			ruta = "/admin/ServletAdminListarLibros";
		}else {
			ruta = "/admin/editarLibro.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
		
		
	}

}
