package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServLetAdminGuardarCambiosUsuario
 */
@WebServlet("/admin/ServLetAdminGuardarCambiosUsuario")
public class ServLetAdminGuardarCambiosUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String ruta = "";
		
		String errorId = "";
		String errorName = "";
		String errorApellidos = "";
		String errorEmail = "";
		String errorPass = "";
		String errorGenero = "";
		String errorFechaNacimiento = "";

		// reccoger todo en forma de string
		String name = request.getParameter("campoNombre");
		String apellidos = request.getParameter("campoApellidos");
		String email = request.getParameter("campoEmail");
		String fecha = request.getParameter("campoFecha");
		String id = request.getParameter("campoId");
		String pass = request.getParameter("campoPass");
		String genero = request.getParameter("campoGenero");
		// Tendriamos que hacer las validaciones

		if (!Validators.validatorNombre(name))
			errorName = "Error en Nombre";
		if (!Validators.validatorApellidos(apellidos))
			errorApellidos = "Error en Apellidos";
		if (!Validators.validatorEmail(email))
			errorEmail = "Error en Email";
		if (!Validators.validatorFecha(fecha))
			errorFechaNacimiento = "Error en Fecha";
		if (pass.isEmpty())
			errorPass = "Error en Pass";
		if (genero.isEmpty())
			errorName = "Error en Genero";
		if (!Validators.validatorNumero(id))
			errorId = "Error en id";

		int idInt = Integer.parseInt(id);

		if (errorApellidos.isEmpty() || errorName.isEmpty() || errorEmail.isEmpty() || errorFechaNacimiento.isEmpty()
				|| errorGenero.isEmpty() || errorId.isEmpty() || errorPass.isEmpty()) {

			Usuario usuarioGuardarCambios = new Usuario(name, apellidos, email, pass, genero, idInt, fecha);

			UsuarioDAO usuariosDAO = new UsuarioDAOImpl();
			usuariosDAO.actualizarUsuario(usuarioGuardarCambios);
			ruta = "/admin/ServletAdminListarUsuarios";
//			RequestDispatcher rd = getServletContext().getRequestDispatcher("admin/ServletAdminListarUuarios");
//			rd.forward(request, response);
		}else {
			ruta = "/admin/editarUsuario.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

}
