package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminEditarCategoria
 */
@WebServlet("/admin/ServletAdminEditarCategoria")
public class ServletAdminEditarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idCategoriaEditar = request.getParameter("id");
		String ruta = "";
		System.out.println("id a editar: " + idCategoriaEditar);
		CategoriaDAO categoriaDAO = new CategoriaDAOImpl();

		if (Validators.validatorNumero(idCategoriaEditar)) {
			int idInt = Integer.parseInt(idCategoriaEditar);
			Categoria categoriaAeditar = categoriaDAO.obtenerCategoriaPorId(idInt);
			System.out.println(categoriaAeditar.toString());
			request.setAttribute("categoriaAeditar", categoriaAeditar);
			ruta = "/admin/editarCategoria.jsp";
		}else {
			ruta = "/admin/ServletAdminListarCategorias";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
		
	}

}
