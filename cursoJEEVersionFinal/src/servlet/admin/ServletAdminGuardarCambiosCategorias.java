package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminGuardarCambiosCategorias
 */
@WebServlet("/admin/ServletAdminGuardarCambiosCategorias")
public class ServletAdminGuardarCambiosCategorias extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nombre = request.getParameter("campoNombre");
		String descripcion = request.getParameter("campoDescripcion");
		String id = request.getParameter("campoId");
		
		String ruta = "";
		
		String errorNombre = "";
		String errorDescripcion = "";
		String errorId = "";
		
		if(!Validators.validatorNombre(nombre)) errorNombre = "Nombre no valido";
		if(descripcion.isEmpty()) errorDescripcion = "Rellene la descripción";
		if(!Validators.validatorNumero(id)) errorId = "Error";
		if(errorDescripcion.isEmpty() || errorNombre.isEmpty()) {
			int idInt = Integer.parseInt(id);
			Categoria editar = new Categoria(idInt, nombre, descripcion);
			CategoriaDAO categoriaDAO = new CategoriaDAOImpl();
			categoriaDAO.actualizarCategoria(editar);
			ruta = "/admin/ServletAdminListarCategorias";
		}else {
			ruta = "/admin/editarCategoria.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

}
