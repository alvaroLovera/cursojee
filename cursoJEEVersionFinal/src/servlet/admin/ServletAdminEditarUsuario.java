package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;

/**
 * Servlet implementation class ServletAdminEditarUsuario
 */
@WebServlet("/admin/ServletAdminEditarUsuario")
public class ServletAdminEditarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id =	request.getParameter("id");
		// Lo suyo seria comprobar que es un numero
		// transformaciones desspues de validar
		int idInt = Integer.parseInt(id);
		// Ahora necesito todos los datos del usuario del que se
		// picho en EDITAR, por lo que como est� en base de datps eso 
		// me lo tiene que dar un DAO
		UsuarioDAO usuariosDAO = new UsuarioDAOImpl();
		Usuario usuarioAeditar = usuariosDAO.obtenerUsuarioPorId(idInt);
		request.setAttribute("usuarioAeditar", usuarioAeditar);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/editarUsuario.jsp");
		rd.forward(request, response);// Si no pongo esto la pantalla del navegador se queda en blanco
	}

}
