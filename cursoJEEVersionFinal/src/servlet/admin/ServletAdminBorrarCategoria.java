package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminBorrarCategoria
 */
@WebServlet("/admin/ServletAdminBorrarCategoria")
public class ServletAdminBorrarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");
		int idInt = Integer.parseInt(id);
		String ruta = "";

		CategoriaDAO categoriaDAO = new CategoriaDAOImpl();
		categoriaDAO.borrarCategoria(idInt);
		ruta = "/admin/ServletAdminListarCategorias";

		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

}
