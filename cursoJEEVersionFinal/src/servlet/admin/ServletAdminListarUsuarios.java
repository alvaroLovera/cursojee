package servlet.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;

/**
 * Servlet implementation class ServletAdminListarUsuarios
 */
@WebServlet("/admin/ServletAdminListarUsuarios")
public class ServletAdminListarUsuarios extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String campoBusqueda = request.getParameter("campoBusqueda");
		String nombreAbuscar = "";
		if(campoBusqueda != null) {
			nombreAbuscar = campoBusqueda;
		}
		// Paginaciones
		int comienzo = 0;
		int cuantos = 10;
		if(request.getParameter("comienzo") != null) {
			comienzo = Integer.parseInt(request.getParameter("comienzo"));
		}
		int siguiente = comienzo + cuantos;
		int anterior = comienzo - cuantos;
		request.setAttribute("siguiente", siguiente);
		request.setAttribute("anterior", anterior);
		// Vamos a pedir al DAO cuantos resultados hay		
		UsuarioDAO usuarioDAO = new UsuarioDAOImpl();	
		List<Usuario> usuarios = usuarioDAO.obtenerUsuariosPorNombreIndicandoComienzoYcuantos(nombreAbuscar,comienzo,cuantos);//obtenerUsuarios();
		int totalResultados = usuarioDAO.obtenerTotalUsuarios(nombreAbuscar);
		request.setAttribute("totalResultados", totalResultados);
		request.setAttribute("usuarios", usuarios);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/usuarios.jsp");
		rd.forward(request, response);
	}

}
