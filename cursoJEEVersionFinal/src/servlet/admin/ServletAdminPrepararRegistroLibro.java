package servlet.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;

/**
 * Servlet implementation class ServletAdminPrepararRegistroLibro
 */
@WebServlet("/admin/ServletAdminPrepararRegistroLibro")
public class ServletAdminPrepararRegistroLibro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//sacando las categorias para poder mostrarlas
		// en un despegable en el formulario de registro
		CategoriaDAO categoriaDAO = new CategoriaDAOImpl();
			List<Categoria> categorias = categoriaDAO.obtenerCategorias();
		request.setAttribute("categorias", categorias);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/libroForm.jsp");
		rd.forward(request, response);
	}

}
