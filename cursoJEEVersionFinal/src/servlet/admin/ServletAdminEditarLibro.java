package servlet.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;
import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletEditarLibro
 */
@WebServlet("/admin/ServletAdminEditarLibro")
public class ServletAdminEditarLibro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idLibroEditar = request.getParameter("id");
		String ruta = "";
		System.out.println("id a editar: " + idLibroEditar);
		LibroDAO libroDAO = new LibroDAOImpl();

		if (Validators.validatorNumero(idLibroEditar)) {
			Libro libroAeditar = libroDAO.obtenerLibroPorId(idLibroEditar);
			CategoriaDAO cDAO = new CategoriaDAOImpl();
			List<Categoria> categorias = new ArrayList<Categoria>();
			categorias = cDAO.obtenerCategorias();
			
			request.setAttribute("categorias", categorias);
			request.setAttribute("libroAeditar", libroAeditar);
			ruta = "/admin/editarLibro.jsp";
		}else {
			ruta = "/admin/ServletAdminListarLibros";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

}
