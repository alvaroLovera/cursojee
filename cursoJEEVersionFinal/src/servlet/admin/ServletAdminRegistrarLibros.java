package servlet.admin;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;
import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServletAdminRegistrarLibros
 */
@MultipartConfig
@WebServlet("/admin/ServletAdminRegistrarLibros")
public class ServletAdminRegistrarLibros extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletAdminRegistrarLibros() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String ruta = "";
		String errorTitulo = "";
		String errorAutor = "";
		String errorEditorial = "";
		String errorPrecio = "";
		String errorNpaginas = "";

		String titulo = request.getParameter("campoTitulo");
		String autor = request.getParameter("campoAutor");
		//String genero = request.getParameter("campoGenero");
		String editorial = request.getParameter("campoEditorial");
		String nPaginas = request.getParameter("campoNpaginas");
		String precio = request.getParameter("campoPrecio");
		String idCategoria = request.getParameter("campoCategoria");
		
		
		int nPaginasInt = 0;
		double precioDouble = 0.0;

		if (!Validators.validatorNombre(titulo)) {
			errorTitulo = "Titulo incorrecto";
		}
		if (!Validators.validatorApellidos(autor)) {
			errorAutor = "Nombre incorrecto";
		}
//		if (!Validators.validatorNombre(genero)) {
//			genero = "Genero Invalido";
//		}
		if (!Validators.validatorNombre(editorial)) {
			errorEditorial = "Editorial incorrecta";
		}
		if (!Validators.validatorNumero(nPaginas)) {
			errorNpaginas = "Numer de paginas incorrecta";
		} else {
			nPaginasInt = Integer.parseInt(nPaginas);
		}
		if (!Validators.validatorPrecio(precio)) {
			errorPrecio = "Precio incorrecto";
		} else {
			precioDouble = Double.parseDouble(precio);
		}

		if (!errorPrecio.isEmpty() || !errorTitulo.isEmpty() || !errorNpaginas.isEmpty() || !errorAutor.isEmpty()
				|| !errorEditorial.isEmpty()) {
			ruta = "/admin/libroForm.jsp";
			request.setAttribute("errorTitulo", errorTitulo);
			request.setAttribute("errorNpaginas", errorNpaginas);
			request.setAttribute("errorPrecio", errorPrecio);
			request.setAttribute("errorAutor", errorAutor);
			request.setAttribute("errorEditorial", errorEditorial);
			request.setAttribute("titulo", titulo);
			request.setAttribute("autor", autor);
//			request.setAttribute("genero", genero);
			request.setAttribute("editorial", editorial);
			request.setAttribute("nPaginas", nPaginas);
			request.setAttribute("precio", precio);
		} else {
			
			int idCategoriaInt = Integer.parseInt(idCategoria);
			
			Libro nuevo = new Libro(titulo, autor, "" , editorial, nPaginasInt, precioDouble, idCategoriaInt);
			try {
				LibroDAO libroDAO = new LibroDAOImpl();
				
				
				int idGenerada = libroDAO.registrarLibro(nuevo);
				System.out.println("Id generada: " + idGenerada);
				// vamos a guardar la foto subida en la carpeta imagenes
				String carpetaSubidas = getServletContext().getRealPath("") + File.separator + "libros";
				File uploadDir = new File(carpetaSubidas);
				if (!uploadDir.exists()) {
					uploadDir.mkdir();
					System.out.println("Creada la carpeta: " + uploadDir);
				}
				System.out.println("Guardado archivo subido en: " + uploadDir);
				Part imagenSubida =	request.getPart("campoImagen");//guardado la foto subida de campo imagen
				imagenSubida.write(carpetaSubidas + File.separator + idGenerada + ".jpg");
				
				ruta = "/admin/ServletAdminListarLibros";
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
				ruta = "/error.jsp";
			}
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
