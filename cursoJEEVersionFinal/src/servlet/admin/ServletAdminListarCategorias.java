package servlet.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.CategoriaDAOImpl;

/**
 * Servlet implementation class ServletAdminListarCategorias
 */
@WebServlet("/admin/ServletAdminListarCategorias")
public class ServletAdminListarCategorias extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Categoria> categorias = new ArrayList<Categoria>();
		CategoriaDAO categoriasDAO = new CategoriaDAOImpl();	
		categorias = categoriasDAO.obtenerCategorias();
		
		request.setAttribute("categorias", categorias);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/categorias.jsp");
		rd.forward(request, response);
	}

}
