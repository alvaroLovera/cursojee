package servlet.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.LibroDAO;
import modelo.LibroDAOImpl;



/**
 * Servlet implementation class ServletBorrarLibro
 */
@WebServlet("/admin/ServletAdminBorrarLibro")
public class ServletAdminBorrarLibro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				String idLibroAborrar = request.getParameter("id");
				System.out.println("id a borrar: " + idLibroAborrar);
				LibroDAO libroDAO = new LibroDAOImpl();
				
				
				libroDAO.borrarLibro(idLibroAborrar);
				
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/ServletAdminListarLibros");
				rd.forward(request, response);
	}

}
