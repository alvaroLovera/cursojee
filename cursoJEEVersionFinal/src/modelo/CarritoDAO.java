package modelo;

import java.util.List;

public interface CarritoDAO {
	void guardarEnCarritoCarrito(Carrito c);

	List<Libro> obtenerLibrosEnCarritoPorUsuarioId(int idUsuario);
	
}
