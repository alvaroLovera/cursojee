package modelo;

public class Libro {

	private String titulo, autor, genero, editorial;
	private int nPaginas, id;
	private double precio;
	private int idCategoria;// Para registro ediciones y otras operaciones
	private Categoria categoria;// Util para listados

	public Libro() {
	}
	



	public Libro(String titulo, String autor, String genero, String editorial, int nPaginas, double precio,
			int idCategoria) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.editorial = editorial;
		this.nPaginas = nPaginas;
		this.precio = precio;
		this.idCategoria = idCategoria;
	}

	public Libro(String titulo, String autor, String genero, String editorial, int nPaginas, int id, double precio,
			int idCategoria) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.editorial = editorial;
		this.nPaginas = nPaginas;
		this.id = id;
		this.precio = precio;
		this.idCategoria = idCategoria;
	}




	public Libro(String titulo, String autor, String genero, String editorial, int nPaginas, int id, double precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.editorial = editorial;
		this.nPaginas = nPaginas;
		this.id = id;
		this.precio = precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public int getnPaginas() {
		return nPaginas;
	}

	public void setnPaginas(int nPaginas) {
		this.nPaginas = nPaginas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	
	
	public Categoria getCategoria() {
		return categoria;
	}




	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}




	@Override
	public String toString() {
		return "Libro [titulo=" + titulo + ", autor=" + autor + ", genero=" + genero + ", editorial=" + editorial
				+ ", nPaginas=" + nPaginas + ", id=" + id + ", precio=" + precio + "]";
	}

}
