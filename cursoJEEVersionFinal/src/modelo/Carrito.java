package modelo;

public class Carrito {
	private int id;
	private int idProducto;
	private int idUsuario;
	private int idPedido;
	private int cantidad;
	
	public Carrito() {
	}

	public Carrito(int idProducto, int idUsuario, int idPedido, int cantidad) {
		super();
		this.idProducto = idProducto;
		this.idUsuario = idUsuario;
		this.idPedido = idPedido;
		this.cantidad = cantidad;
	}

	public Carrito(int id, int idProducto, int idUsuario, int idPedido, int cantidad) {
		super();
		this.id = id;
		this.idProducto = idProducto;
		this.idUsuario = idUsuario;
		this.idPedido = idPedido;
		this.cantidad = cantidad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "Carrito [id=" + id + ", idProducto=" + idProducto + ", idUsuario=" + idUsuario + ", idPedido="
				+ idPedido + ", cantidad=" + cantidad + "]";
	}
	
}
