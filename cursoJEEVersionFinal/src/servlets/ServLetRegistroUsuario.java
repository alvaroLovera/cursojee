package servlets;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;
import utils.Validators;

/**
 * Servlet implementation class ServLetRegistroUsuario
 */
@MultipartConfig
@WebServlet("/ServLetRegistroUsuario")
public class ServLetRegistroUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServLetRegistroUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String ruta = "";
		String errorNombre = "";
		String errorApellidos = "";
		String errorFechaNacimiento = "";
		String errorPass = "";
		String errorEmail = "";

		String nombre = request.getParameter("campoNombre");
		String apellidos = request.getParameter("campoApellidos");
		String email = request.getParameter("campoEmail");
		String pass = request.getParameter("campoPass");
		String genero = request.getParameter("campoGenero");
		String fechaNacimiento = request.getParameter("campoFecha");

		if (!Validators.validatorNombre(nombre)) {
			errorNombre = "Nombre incorrecto";
		}
		if (!Validators.validatorApellidos(apellidos)) {
			errorApellidos = "Nombre incorrecto";
		}
		if (!Validators.validatorEmail(email)) {
			errorEmail = "Email Invalido";
		}
		if (!Validators.validatorFecha(fechaNacimiento)) {
			errorFechaNacimiento = "Fecha incorrecta";
		}

		if (!errorEmail.isEmpty() || !errorNombre.isEmpty() || !errorPass.isEmpty() || !errorApellidos.isEmpty()
				|| !errorFechaNacimiento.isEmpty()) {
			ruta = "/registro.jsp";
			request.setAttribute("errorNombre", errorNombre);
			request.setAttribute("errorPass", errorPass);
			request.setAttribute("errorEmail", errorEmail);
			request.setAttribute("errorApellidos", errorApellidos);
			request.setAttribute("errorFechaNacimiento", errorFechaNacimiento);
			request.setAttribute("nombre", nombre);
			request.setAttribute("email", email);
			request.setAttribute("apellidos", apellidos);
			request.setAttribute("fechaNacimiento", fechaNacimiento);
			request.setAttribute("pass", pass);
		} else {
			Usuario nuevo = new Usuario(nombre, apellidos, email, pass, genero, fechaNacimiento);
			try {
				UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
				int idGenerada = usuarioDAO.registrarUsuario(nuevo);
				System.out.println("Id generada: " + idGenerada);
				// vamos a guardar la foto subida en la carpeta imagenes
				String carpetaSubidas = getServletContext().getRealPath("") + File.separator + "usuarios";
				File uploadDir = new File(carpetaSubidas);
				if (!uploadDir.exists()) {
					uploadDir.mkdir();
					System.out.println("Creada la carpeta: " + uploadDir);
				}
				System.out.println("Guardado archivo subido en: " + uploadDir);
				Part imagenSubida =	request.getPart("campoImagen");//guardado la foto subida de campo imagen
				imagenSubida.write(carpetaSubidas + File.separator + idGenerada + ".jpg");		
				ruta = "/index.jsp";
			} catch (Exception e) {
				ruta = "/registro.jsp";
			}
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
