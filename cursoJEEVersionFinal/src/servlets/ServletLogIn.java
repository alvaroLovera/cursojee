package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.UsuarioDAO;
import modelo.UsuarioDAOImpl;

/**
 * Servlet implementation class ServletLogIn
 */
@WebServlet("/ServletLogIn")
public class ServletLogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogIn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("campoEmail");
		String pass = request.getParameter("campoPass");
		
		UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
		int idUsuario = usuarioDAO.getIdUsuario(email, pass);
		String ruta = "";
		if(idUsuario > 0) {
			request.setAttribute("mensaje","Identificado correctamente");
			ruta = "/zonaPrivada.jsp";
			request.getSession().setAttribute("id", idUsuario);
			request.getSession().setAttribute("email", email);
			request.getSession().setAttribute("logued", "ok");
			
		} else {
			request.setAttribute("mensaje", "Email o contrasena incorrectos");
			ruta = "/login.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
