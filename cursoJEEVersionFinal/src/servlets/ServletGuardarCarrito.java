package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Carrito;
import modelo.CarritoDAO;
import modelo.CarritoDAOImpl;
import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;

/**
 * Servlet implementation class ServletGuardarCarrito
 */
@WebServlet("/ServletGuardarCarrito")
public class ServletGuardarCarrito extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idUsuario = (int) request.getSession().getAttribute("id");
		String idLibro = request.getParameter("idLibro");
		
		CarritoDAO carritoDAO = new CarritoDAOImpl();
		LibroDAO libroDAO = new LibroDAOImpl();
		Libro l = libroDAO.obtenerLibroPorId(idLibro);
		Carrito c = new Carrito();
		c.setCantidad(1);
		c.setIdProducto(l.getId());
		c.setIdUsuario(idUsuario);
		
		carritoDAO.guardarEnCarritoCarrito(c);
		
		request.setAttribute("compra", "Se ha guardado en su carrito: " + l.getTitulo());
		
		RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
		rd.forward(request, response);
	}

}
