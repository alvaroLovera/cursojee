package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Libro;
import modelo.LibroDAO;
import modelo.LibroDAOImpl;

/**
 * Servlet implementation class ServletListarLibros
 */
@WebServlet("/ServletListarLibros")
public class ServletListarLibros extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LibroDAO libDAO = new LibroDAOImpl();
		List<Libro> libros = new ArrayList<Libro>();
		
		libros = libDAO.obtenerLibros();
		
		int totalResultados = libDAO.obtenerTotalLibros("");
		request.setAttribute("totalResultados", totalResultados);
		request.setAttribute("libros", libros);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/listadoLibrosCliente.jsp");
		rd.forward(request, response);
	}

}
