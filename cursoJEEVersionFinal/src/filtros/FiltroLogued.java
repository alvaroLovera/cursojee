package filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FiltroLogued implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest requestPrimitivo, ServletResponse responsePrimitivo, FilterChain chain) 
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) requestPrimitivo;
		HttpServletResponse response = (HttpServletResponse) responsePrimitivo;
			
		if(request.getSession().getAttribute("logued") !=null && request.getSession().getAttribute("logued").equals("ok")) {
			// Si se cumple este if permito que la sesion siga adelante eso debe ir en el web.xml
			chain.doFilter(request, response);
		}else {
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/login.jsp");
			request.setAttribute("mensaje", "Debe loguearse correctamente");
			rd.forward(request, response);		
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}
}
