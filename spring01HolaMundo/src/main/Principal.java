package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.PeliculasDAOImpl;

public class Principal {

	public static void main(String[] args) {
		
//		Para crar un contenedor de Spring con lo puesto en el aplicationContext.xml
		ApplicationContext contenedor = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		// Ahora mismo contenedor tiene todas las bean/objetos
		// especificados en applicationContext.xml
		
		//a lo largo de la aplicacion se lo pedimos al contenedor
		//de spring
		PeliculasDAOImpl pDAO = contenedor.getBean("peliculasDAO",PeliculasDAOImpl.class);
		// Asi obtenido la Bean de id="peliculasDAO"
		// la cual tiene que ser objeto de la clase PeliculasDAOImpl
		pDAO.borrarPelicula(5);
		
		//Aunque parezca lo mas logico pedir las beans por id lo mas normal es pedirlas por tipo de dato
		PeliculasDAOImpl pDAO2 = contenedor.getBean(PeliculasDAOImpl.class);
		pDAO2.borrarPelicula(33);
		// Esta ultima forma es la mas comun ya que es inusual tener
		// m�s de una bean/objeto de un mismo tipo en el contenedor
		// ej: El cotenedor solo deberia tener un PeliculasDAO
		//En contenedor solo deberia tener un UsuarioDAO
		// etc...
		
	}

}
