package main;

public class GestorArchivosVideo {
	
	// Inicio Singleton
	private static final GestorArchivosVideo INSTANCIA = new GestorArchivosVideo();
	
	private GestorArchivosVideo() {
		// Este construcctor es privado
	}
	public static GestorArchivosVideo obtenerInstancia() {
		return INSTANCIA;
	}
	
	
	// Fin sigleton
	// Resto de codigo de la clase
}
