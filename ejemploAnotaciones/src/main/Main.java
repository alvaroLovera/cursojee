package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main {

	public static void main(String[] args) {
		
		// Las anotaciones no tienen codigo por detro
		// se usan para marcar clases, m�todos etc.
		
		// Y ahora otro codigo escnea que cosas tienen que anotaciones y ejecuta
		/// lo que haya que ejecutar en consecuenccia

		
		Apuesta a = new Apuesta();
		Apuesta a2 = new Apuesta();
		Juego j = new Juego();
		
		List<Object> objetos = new ArrayList<Object>(); 
		objetos.add(a);
		objetos.add(a2);
		objetos.add(j);
		procesarAnotaciones(objetos);
	}

	// Una anotacion unicamente marca algo, una anotacion de por si no tiene codigo a 
	// ejecutar otro codigo ejecutara lo que quiera ejecutar
	// si algo fue anotado con una anotacon
	private static void procesarAnotaciones(List<Object> objetos) {
		
		for (Object object : objetos) {
			Servicio anotacionServico = object.getClass().getAnnotation(Servicio.class);
			if(anotacionServico != null) {
				System.out.println("Detectada anotacion @Servico");
			}
		}
		
	}

}
