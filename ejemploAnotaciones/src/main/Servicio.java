package main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// Con lo siguiente decimos que la anotacion Servicio se use en tiempo de ejecucion
@Retention(RetentionPolicy.RUNTIME)
public @interface Servicio {
	
	// Una anotacion no puede contener metodos con c�digo
	
}
