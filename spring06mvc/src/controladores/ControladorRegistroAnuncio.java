package controladores;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.AnunciosDAO;
import modelo.Anuncio;

@Controller
public class ControladorRegistroAnuncio {
	
	private AnunciosDAO miAnunciosDAO;
	
	@RequestMapping("prepararRegistro")
	public String prepararRegistro(Map model) {
		System.out.println("Ejecutando registro de anuncio");
		// Nuevo anuncio es el commandName del form de Spring y
		// no es,as quie el objeto que esta esperando el form
		// para poder mostrar su informacion en el
		// Esto es obligatorio y si le damos obligatoriamente 
		// un new Anuncio(), al form le llega un nuevo 
		// objeto de la clase Anuncio sin informacion
		model.put("nuevoAnuncio", new Anuncio());
		return "/WEB-INF/jsps/registrarAnuncio.jsp";
	}
	
	@RequestMapping("guardarAnuncio")
	public String guardarAnuncio(Anuncio nuevoAnuncio) {
		// Este metodo reibe auttomaticamente lo que ha introducido el usuario en el form
		System.out.println("He recibido" + nuevoAnuncio.toString());
		miAnunciosDAO.registrarAnuncio(nuevoAnuncio);
		System.out.println("Anuncio Registrado OK");
		return "/WEB-INF/jsps/inicio.jsp";
	}

	
	
	public void setMiAnunciosDAO(AnunciosDAO miAnunciosDAO) {
		this.miAnunciosDAO = miAnunciosDAO;
	}

	
}
