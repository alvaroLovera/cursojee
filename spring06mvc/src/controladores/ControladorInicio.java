package controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ControladorInicio {
	
	//De esta forma el siguiente m�todo cuando el usuario 
	// acceda a la ruta inicio
	// cuando pinche en el enlace de index.jsp
	@RequestMapping("inicio")
	public String inicio() {		
		System.out.println("Se ejecuta el metodo inicio");
		return "/WEB-INF/jsps/inicio.jsp";
	}
	
}
