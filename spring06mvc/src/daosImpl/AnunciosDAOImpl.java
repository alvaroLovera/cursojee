package daosImpl;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import daos.AnunciosDAO;
import modelo.Anuncio;

public class AnunciosDAOImpl implements AnunciosDAO{

	// A diferencia de otros DAO realizados anteriormente 
	// vamos a usar Spring JDBC pata falicitarnos la tarea con BBDD
	// para ello la clase actual necesita un DataSource. 
	// un DataSource es un elemento que tiene informacion para 
	// conectarse ccon una BBDD
	
	private DataSource dataSource;
	private SimpleJdbcInsert simpleInsert;
	
	@Override
	public void registrarAnuncio(Anuncio a) {
		// Vamos a usar el simple jdbc insert, que es un recurso
		// de spring jdbc, el cual ha necesitado de un data source
		Map<String, Object> valores = new HashMap<String, Object>();
		valores.put("titulo", a.getTitulo());
		valores.put("email", a.getEmail());
		valores.put("descripcion", a.getDescripcion());
		simpleInsert.execute(valores);
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		simpleInsert = new SimpleJdbcInsert(dataSource);
		simpleInsert.setTableName("tabla_anuncios");
	}
	
	
	

}
