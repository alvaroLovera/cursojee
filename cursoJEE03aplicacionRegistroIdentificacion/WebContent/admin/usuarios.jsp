<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
Listado de usuarios registrados:<br/>
<c:forEach var="usuario" items="${usuarios}" >
    nombre: ${usuario.nombre}
    email: ${usuario.email}
</c:forEach>
</body>
</html>