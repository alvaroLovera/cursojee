package modelo;

import java.util.ArrayList;

// Definimos las operaciones que se peden hacer en BBDD
// otra clase indicar� con su codigo que hacen dichas operaciones
// esa clase es la que implementa el DAO
public interface UsuariosDAO {

	public void registrarUsuario(Usuario u);
	public int obtenerIdUsuario(String email, String pass);
	public ArrayList<Usuario> obtenerUsuarios();
	
	
	
}
