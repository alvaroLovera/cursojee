
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class MasterDAO {

	private String url = "jdbc:mysql://localhost:3306/app_web?useUnicode=true&useJDBCCompliantTimezoneShift="
			+ "true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String usuario = "root";
	private String pass = "admin";
	//Protecte el elemento se puede usar en la clase actual y en las que hereden de ella
	protected Connection conexion;
	
	public MasterDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	protected void conectar() {
		try {
			conexion = DriverManager.getConnection(url,usuario,pass);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	protected void desconectar() {
		try {
			conexion.close();
		} catch (SQLException e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
		}
	}//end desconectar
	
}
