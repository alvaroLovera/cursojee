package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuariosDAOImpl extends MasterDAO implements UsuariosDAO {

	@Override
	public void registrarUsuario(Usuario u) {
		conectar();
		
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.INSERT_USUARIO_SQL);
			ps.setString(1, u.getNombre());
			ps.setString(2, u.getEmail());
			ps.setString(3, u.getPass());
			ps.execute();	
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
	}

	@Override
	public int obtenerIdUsuario(String email, String pass) {
		conectar();
		int idUsuario = -1;
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_ID_USUARIO);
			ps.setString(1, email);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				idUsuario = rs.getInt("id");
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		return idUsuario;
	}

	@Override
	public ArrayList<Usuario> obtenerUsuarios() {
	
		conectar();
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_OBTENER_USUARIOS);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Usuario u = new Usuario();
				u.setNombre(rs.getString("nombre"));
				u.setId(rs.getInt("id"));
				u.setEmail(rs.getString("email"));
				usuarios.add(u);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		desconectar();
		
		return usuarios;
	}

}
