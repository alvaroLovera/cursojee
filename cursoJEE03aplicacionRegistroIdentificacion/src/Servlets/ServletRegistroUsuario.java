package servlets;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import modelo.UsuariosDAO;
import modelo.UsuariosDAOImpl;
import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;

/**
 * Servlet implementation class ServletRegistroUsuario
 */
@WebServlet("/ServletRegistroUsuario")
public class ServletRegistroUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletRegistroUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Ruta a la que el usuario ira cuando este Servlet
		// Acabe de hacer su trabajo
		String ruta = "";
		String errorNombre = "";
		String errorPass = "";
		String errorEmail = "";
		
		String nombre = request.getParameter("campoNombre");
		String email = request.getParameter("campoEmail");
		String pass = request.getParameter("campoPass");
		// Todo lo recibido de un form se recoge como String
		// Una vez recogido todo lo necesario ya puedo validar 
		// y hacer transformaciones si es necesario parte de validaciones:
		
		//Validaciones
		String reglaNombre = "[a-z A-Z��������������]{3,20}";
		String reglaEmail = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$";
		// En Java si queremos una sola barra \ hay que poner \\
		
		String reglaPass = "[a-zA-A 0-9]{3,20}";
		
		Pattern p = Pattern.compile(reglaNombre);
		Matcher m = p.matcher(nombre);
		
		if(!m.matches()) {
			//Redirigir a registro indicando el error 	
			errorNombre = "Nombre incorrecto";
		}
		//Podemos hacer lo mismo en una sola linea
		if(!Pattern.compile(reglaEmail).matcher(email).matches()) {
			errorEmail = "Email no v�lido";
		}
		if(!Pattern.compile(reglaPass).matcher(pass).matches()) {
			errorPass = "Pass no v�lido";
		}
		
		//Ahora deberias aplicar validaciones
		
		if(!errorEmail.isEmpty() || !errorNombre.isEmpty() || !errorPass.isEmpty()) {
			ruta = "/registrarme.jsp";
			request.setAttribute("errorNombre", errorNombre);
			request.setAttribute("errorPass", errorPass);
			request.setAttribute("errorEmail", errorEmail);
			request.setAttribute("nombre", nombre);
			request.setAttribute("email", email);
			request.setAttribute("pass", pass);
		}else {			
			//Si voy a acceder a BBDD debo llamar a la capa de 
			// acceso a datos
			// UsuariosDAOImpl usuariosDAO = new UsuariosDAOImpl();
			// es mucho mas comun ver lo siguinete:
			UsuariosDAO usuariosDAO = new UsuariosDAOImpl();
			// por varios motivos
			// si en UsuariosDAOImpl creo nuevas metofdos que no 
			//tengan que ver con las operaciones que he definido en UsuariosDAO, de esta forma
			// usuariosDAO solo puede invocar lo definido en UsuariosDAO;
			
			Usuario nuevo = new Usuario(nombre, pass, email);
			try {
				usuariosDAO.registrarUsuario(nuevo);
				ruta = "/registroOk.jsp";
			} catch (Exception e) {
				ruta = "/error.jsp";
			}
		}// else comprobar que no ha habido errores
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
		
	}// end doGet

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
