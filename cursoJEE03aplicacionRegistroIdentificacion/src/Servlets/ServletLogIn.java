package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.UsuariosDAO;
import modelo.UsuariosDAOImpl;

/**
 * Servlet implementation class ServletLogIn
 */
@WebServlet("/ServletLogIn")
public class ServletLogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogIn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("campoEmail");
		String pass = request.getParameter("campoPass");
		System.out.println("Identificar: "+email+" "+pass);
		
		UsuariosDAO usuarioDAO = new UsuariosDAOImpl();
		int idUsuario = usuarioDAO.obtenerIdUsuario(email, pass);
		// Si hemos obtenido un Id valido entendemos que el usuario se ha indentificado carrectamente
		// entendsemo que el email y pass introducidos son correctos
		String ruta = "";
		if(idUsuario > 0) {
			request.setAttribute("mensaje", " identificado correctamente");
			ruta = "/privada.jsp";
			// Con lo siguiente ,arcamos que el usuario actual
			// se ha identificado correctamente
			request.getSession().setAttribute("id", idUsuario);
			request.getSession().setAttribute("email", email);
		}else {
			request.setAttribute("mensaje", " Email o pass incorrectos");
			ruta = "/login.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(ruta);
		rd.forward(request, response);
		//Asi indicamos que la pericion siga proceandose en la ruta indicada
	}// end doGet 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Con la siguiente linea tanto si sw ha llegado una peticion 
		//  por get como por post, la peticion sera atendida en el doGet
		doGet(request, response);
	}

}
