package main;

public class Main {

	public static void main(String[] args) {
		
		saludar("hola", "que tal" );
		
	}
	
	// los puntos suspensibos te indican un numero de parametros identerminado
	private static void saludar(String... saludos) {
		for (String string : saludos) {
			System.out.println(string);
		}
	}

}
