package main;

public class Main {

	public static void main(String[] args) {
	
		// En java los tipos basicos de dato se tratan de forma distinta a los objetos
		int x = 10;
		Usuario u = new Usuario("Juan", "juan@gmail.com");
		sumaDos(x);
		cambiarNombre(u, "Javier");
		System.out.println("x : " + x);
		System.out.println("nombre de u: " + u.getNombre());
		// Al dar un tipo basico de dato original, por eso da igual que le
		// sumo dos o lo que sea. El dato original no se modifica
		// sin embargo como se puede ver este ejemplo, con los objetos pasa lo contrario
	}
	
	private static void sumaDos(int x) {
		x = x + 2;
	}
	
	private static void cambiarNombre(Usuario u, String nombre) {
		u.setNombre(nombre);
	}

}
