package test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import daos.AnunciosDAO;
import modelo.Anuncio;

// Lo siguiente es para lanzar una unidad de test usando el contenedor de Spring indicado
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"spring-context-test.xml"})
public class RegistrarMilAnuncios {

	@Autowired
	private AnunciosDAO anunciosDAO;
	
	
	
	@Test
	public void test() throws InterruptedException {
		System.out.println("Se inicioa la unidad de testeo");
		// Generalmente tenemos dos tipos de testing :
		// testeo unitario : Codigo que cmprueba funciones del propio sistema 
		//testeo de Integracion: codigo que comprueba como funciona nuestro sistema con otros
		Anuncio anuncio = new Anuncio();
		String titulo = "pruebas";
		anuncio.setDescripcion("Descripcion de pruebas");
		anuncio.setEmail("pruebas@pruebas.com");
		anuncio.setCiudad("Burgos");
		anuncio.setDescripcion("AAA");
		anuncio.setEstado("Sin usar");
		anuncio.setPrecio(2);
		anuncio.setTelefono("123456");
//		anunciosDAO.registrarAnuncio(anuncio);
		for (int i = 0; i < 10; i++) {
			anuncio.setTitulo(titulo+i+"");
			anunciosDAO.registrarAnuncio(anuncio);
			Thread.sleep(100);
		}
		
		
		System.out.println("Fin de la unidad de testeo");
	}

}
