package daosImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import constantes.ConstantesSQL;
import constantes.NombresTablas;
import daos.UsuariosDAO;
import mappers.ServicioMapper;
import mappers.UsuarioMapper;
import modelo.Usuario;

@Repository
public class UsuariosDAOImpl implements UsuariosDAO {
	@Autowired
	private DataSource dataSource;
	private SimpleJdbcInsert simpleInsert;
	private JdbcTemplate template;

	@PostConstruct
	public void prepararElementos() {
		simpleInsert = new SimpleJdbcInsert(dataSource);
		simpleInsert.setTableName(NombresTablas.TABLA_USUARIOS);
		simpleInsert.usingGeneratedKeyColumns("id");
		template = new JdbcTemplate(dataSource);
	}

	@Override
	public void registrarUsuario(Usuario u) {
		Map<String, Object> valores = new HashMap<String, Object>();
		valores.put("nombre", u.getNombre());
		valores.put("email", u.getEmail());
		valores.put("pass", u.getPass());
		simpleInsert.execute(valores);

	}

	@Override
	public Usuario obtenerUsuarioPorMailYpass(String email, String pass) {
		Usuario u = null;
		try {
			u = template.queryForObject(ConstantesSQL.SQL_OBTENER_USARIO_POR_EMAIL_Y_PASS, new UsuarioMapper(), email, pass);
		} catch (Exception e) {
			System.out.println("usuario no encontrado");
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public List<Usuario> obtenerUsuarios() {
		List<Usuario> usuarios = template.query(ConstantesSQL.SQL_OBTENER_USUARIOS, new UsuarioMapper());
		return usuarios;
	}

	@Override
	public void borrarUsuario(int id) {	
		template.update(ConstantesSQL.SQL_BORRAR_USUARIO,id);
	}

	@Override
	public Usuario obtenerUsuarioPorId(int id) {
		Usuario u = template.queryForObject(ConstantesSQL.SQL_OBTENER_USUARIO_POR_ID, new UsuarioMapper(), id);
		return u;
	}

	@Override
	public void actualizarUsuario(Usuario usuarioAeditar) {
		
		template.update(ConstantesSQL.SQL_ACTUALIZAR_USUARIO,
				usuarioAeditar.getNombre(),
				usuarioAeditar.getEmail(),
				usuarioAeditar.getPass(),
				usuarioAeditar.getId());
		
	}
	
	

}
