package daosImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import constantes.ConstantesSQL;
import constantes.NombresTablas;
import daos.ServiciosDAO;
import mappers.AnuncioMapper;
import mappers.ServicioMapper;
import modelo.Anuncio;
import modelo.Servicio;

@Component // @Repository
public class ServiciosDAOImpl implements ServiciosDAO {
	@Autowired
	private DataSource dataSource;
	private SimpleJdbcInsert simpleInsert;
	private JdbcTemplate template;

	@Override
	public int registrarServicio(Servicio s) {

		int idGenerada = -1;
		Map<String, Object> valores = new HashMap<String, Object>();
		valores.put("nombre", s.getNombre());
		valores.put("profesion", s.getProfesion());
		valores.put("descripcion", s.getDescripcion());
		valores.put("tarea", s.getTarea());
		valores.put("ciudad", s.getCiudad());
		// simpleInsert.execute(valores);
		return	idGenerada = simpleInsert.usingGeneratedKeyColumns("id").executeAndReturnKey(valores).intValue();
	}

	@Override
	public List<Servicio> obtenerServicios() {
		List<Servicio> servicios = template.query(ConstantesSQL.SQL_OBTENER_SERVICIOS, new ServicioMapper());
		return servicios;
	}

	// Una vez asignadas todas las propiedades, ya sea por @Autowired @Resouerce o
	// de cualquier otra forma se ejecuta
	// el metodo @PostConstruct
	@PostConstruct
	public void prepararSimpleInsertYjdbcTemplate() {
		simpleInsert = new SimpleJdbcInsert(dataSource);
		simpleInsert.setTableName(NombresTablas.TABLA_SERVICIOS);
		template = new JdbcTemplate(dataSource);
	}

	@Override
	public void borrarServicio(int id) {
		template.update(ConstantesSQL.SQL_BORRAR_SERVICIO,id);
	}

	@Override
	public Servicio obtenerServicioPorId(int id) {
		Servicio s = template.queryForObject(ConstantesSQL.SQL_OBTENER_SERVICIO_POR_ID, new ServicioMapper(),id);	
		return s;
	}

	@Override
	public void actualizarServicio(Servicio servicioAeditar) {
		template.update(ConstantesSQL.SQL_ACTUALIZAR_SERVICIO,
				servicioAeditar.getNombre(),
				servicioAeditar.getProfesion(),
				servicioAeditar.getTarea(),
				servicioAeditar.getDescripcion(),
				servicioAeditar.getCiudad(),
				servicioAeditar.getId());
	}

	@Override
	public List<Servicio> obtenerServiciosPorTituloIndicandoComienzoYCunatos(String campoBusqueda, Integer comienzo,
			int cuantos) {
		List<Servicio> servicios = template.query(ConstantesSQL.SQL_OBTENER_SERVICIOS_INDICANDO_COMIENZO_Y_CUANTOS, 
				new ServicioMapper(), "%" + campoBusqueda + "%", comienzo, cuantos);
		
		return servicios;
	}
	
	
	

}
