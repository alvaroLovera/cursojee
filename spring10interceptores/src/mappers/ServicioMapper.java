package mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import modelo.Servicio;

public class ServicioMapper implements RowMapper<Servicio>{

	@Override
	public Servicio mapRow(ResultSet rs, int numeroFila) throws SQLException {		
		Servicio servicio = new Servicio();
		servicio.setId(rs.getInt("id"));
		servicio.setNombre(rs.getString("nombre"));
		servicio.setProfesion(rs.getString("profesion"));
		servicio.setTarea(rs.getString("tarea"));
		servicio.setCiudad(rs.getString("ciudad"));		
		return servicio;
	}

}
