package controladores;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.AnunciosDAO;
import modelo.Anuncio;

@Controller
public class ControladorInicio {
	
	// Si desde aqui quiero usar AnunciosDAO , pues lo pido
	@Autowired
	private AnunciosDAO miAnunciosDAO;
	
	//de esta forma el siguiente metodo se ejecuta
	//cuando el usuario acceda a la ruta inicio
	//cuando pinche en el enlace de index.jsp
	@RequestMapping("inicio")
	public String inicio(Map model, String campoBusqueda, Integer comienzo) {
		System.out.println("Busqueda" + campoBusqueda);
		System.out.println("comienzo" + comienzo);
		
		if(comienzo == null) {
			comienzo = 0;
		}
		if(campoBusqueda == null) {
			campoBusqueda = "";
		}
		
		int cuantos = 5;
		int siguiente = comienzo + cuantos;
		int anterior = comienzo - cuantos;
		
		model.put("anterior", anterior);
		model.put("siguiente", siguiente);
		model.put("campoBusqueda", campoBusqueda);
		
		List<Anuncio> anuncios = miAnunciosDAO.obtenerAnunciosPorTituloIndicandoComienzoYCunatos(campoBusqueda, comienzo, cuantos);//obntenerAnuncios("");
		model.put("anuncios", anuncios);
		// Lo siguiente lleva a la vista inicio.jsp
		// de las vistas gracias a la bean que acabammos de insertar en el 
		// dispatcher-servlet.xml
		return "inicio";
	}

	

		
	
}
