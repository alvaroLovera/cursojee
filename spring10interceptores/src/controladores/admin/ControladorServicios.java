package controladores.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import daos.ServiciosDAO;
import modelo.Anuncio;
import modelo.Servicio;

@Controller
public class ControladorServicios {
	
	@Autowired
	private ServiciosDAO serviciosDO;
	
	@RequestMapping("admin/servicios")
	public String adminServicios(Map model) {
		List<Servicio> servicios = serviciosDO.obtenerServicios();
		model.put("servicios",servicios);
		
		return "admin/servicios";
	}
	
	@RequestMapping("admin/borrarServicio")
	public String adminBorrarServicio(int id, Map model) {
		serviciosDO.borrarServicio(id);
		return adminServicios(model);
	}
	
	@RequestMapping("admin/editarServicio")
	public String adminEditarServicio(int id, Map model) {
		Servicio servicioAeditar = serviciosDO.obtenerServicioPorId(id);
		model.put("servicioAeditar", servicioAeditar);
		return "admin/editarServicio";
	}

	@RequestMapping("admin/actualizarServicio")
	public String adminActualizarServicio(Servicio servicioAeditar, Map model) {
			
		int id = -1;	
		CommonsMultipartFile uploaded = servicioAeditar.getFichero1();				
		serviciosDO.actualizarServicio(servicioAeditar);			
		id = servicioAeditar.getId();		
		File localFile = new File("C://subidasJava/" + id + ".jpg");
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(localFile);
			os.write(uploaded.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
		return adminServicios(model);
	}
	
}
