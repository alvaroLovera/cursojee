package controladores.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import daos.UsuariosDAO;
import modelo.Usuario;

@Controller
public class ControladorAdminUsuarios {

	@Autowired
	private UsuariosDAO usuariosDAO;
	
	@RequestMapping("admin/usuarios")
	public String adminUsuarios(Map model) {
		List<Usuario> usuarios = usuariosDAO.obtenerUsuarios();		
		model.put("usuarios", usuarios);
		return "admin/usuarios";
	}
	
	@RequestMapping("admin/borrarUsuario")
	public String adminBorrarUsuario(int id, Map model) {
		usuariosDAO.borrarUsuario(id);
		return adminUsuarios(model);
	}
	
	@RequestMapping("admin/editarUsuario")
	public String adminEditarUsuarios(int id, Map model) {
		Usuario usuarioAeditar = usuariosDAO.obtenerUsuarioPorId(id);
		model.put("usuarioAeditar", usuarioAeditar);
		return "admin/editarUsuario";
	}

	@RequestMapping("admin/actualizarUsuario")
	public String adminActualizarUsuarios(Usuario usuarioAeditar, Map model) {					
		usuariosDAO.actualizarUsuario(usuarioAeditar);	
		return adminUsuarios(model);
	}
	
}
