package controladores;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import daos.ServiciosDAO;
import daosImpl.ServiciosDAOImpl;
import modelo.Anuncio;
import modelo.Servicio;

@Controller
public class ControladorRegistroServicio {

	@Autowired
	private ServiciosDAO serviciosDAO;
	@Autowired
	private ControlladorServiciosInicio controladorServicioInicio;

	@RequestMapping("prepararServicio")
	public String prepararServicio(Map model) {		
		model.put("nuevoServicio",new Servicio());		
		return "registroServicio";
	}
	

	@RequestMapping("guardarServicio")
	public String guardarServicio( @ModelAttribute("nuevoServicio") @Valid Servicio nuevoServicio, BindingResult result, Map model) {

		System.out.println("he recibido: " + nuevoServicio.toString());
		if (result.hasErrors()) {
			//Hay que volver a mandar un objeto al form con lo que queramos que muestre
			model.put("nuevoServicio", nuevoServicio);
			return "registrarAnuncio";
		} else {
			int id = -1;
			CommonsMultipartFile uploaded1 = nuevoServicio.getFichero1();
			CommonsMultipartFile uploaded2 = nuevoServicio.getFichero2();
			CommonsMultipartFile uploaded3 = nuevoServicio.getFichero3();
			id = serviciosDAO.registrarServicio(nuevoServicio);
			
			File localFile1 = new File("C://subidasJava/" + id + "a.jpg");
			File localFile2 = new File("C://subidasJava/" + id + "b.jpg");
			File localFile3 = new File("C://subidasJava/" + id + "c.jpg");
			FileOutputStream os = null;
			try {
				os = new FileOutputStream(localFile1);
				os.write(uploaded1.getBytes());
				os = new FileOutputStream(localFile2);
				os.write(uploaded2.getBytes());
				os = new FileOutputStream(localFile3);
				os.write(uploaded3.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			System.out.println("anuncio registrado ok");
			return controladorServicioInicio.serviciosInicio(model,"",0);
		}
	}
}
