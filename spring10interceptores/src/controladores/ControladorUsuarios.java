package controladores;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.UsuariosDAO;
import modelo.Usuario;

/**
 * 
 * @author Alvaro Controlador que atiende las rutas: + login + registrarUsuario
 *         + guardarUsuario
 *
 */
@Controller
public class ControladorUsuarios {
	@Autowired
	private UsuariosDAO usuariosDAO;

	@RequestMapping("login")
	public String login() {
		return "login";
	}

	@RequestMapping("identificarUsuario")
	public String identificarUsuario(HttpServletRequest request, String email, String pass) {
		System.out.println(email + "  " + pass);
		Usuario u = usuariosDAO.obtenerUsuarioPorMailYpass(email, pass);
		if (u == null) {
			return "login";
		} else {
			request.getSession().setAttribute("idUsuario", u.getId());
			request.getSession().setAttribute("nombre", u.getNombre());
			request.getSession().setAttribute("email", u.getEmail());
			request.getSession().setAttribute("pass", u.getPass());
			return "loginCorrecto";
		}
	}

	@RequestMapping("registrarUsuario")
	public String registrarUsario(Map model) {
		model.put("nuevoUsuario", new Usuario());
		return "registrarUsuario";
	}

	@RequestMapping("guardarUsuario")
	public String guardarUsuario(Usuario nuevoUsuario) {
		usuariosDAO.registrarUsuario(nuevoUsuario);
		return "registroCorrecto";
	}

}
