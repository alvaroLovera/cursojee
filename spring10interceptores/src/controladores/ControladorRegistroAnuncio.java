package controladores;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import daos.AnunciosDAO;
import modelo.Anuncio;

@Controller
public class ControladorRegistroAnuncio {

	@Resource
	private AnunciosDAO miAnunciosDAO;
	@Resource
	private ControladorInicio controladorInicio;

	@RequestMapping("prepararRegistro")
	public String prepararRegistro(Map model) {
		System.out.println("preparar registro de anuncio");
		model.put("nuevoAnuncio", new Anuncio());
		return "registrarAnuncio";
	}// end prepararregistro

	@RequestMapping("guardarAnuncio")
	public String guardarAnuncio(@ModelAttribute("nuevoAnuncio") @Valid Anuncio nuevoAnuncio, BindingResult result,
			Map model) {
		System.out.println("he recibido: " + nuevoAnuncio.toString());
		if (result.hasErrors()) {
			model.put("nuevoAnuncio", nuevoAnuncio);
			return "registrarAnuncio";
		} else {
			CommonsMultipartFile uploaded = nuevoAnuncio.getFichero();
			File localFile = new File("C://subidasJava/" + uploaded.getOriginalFilename());
			FileOutputStream os = null;
			try {
				os = new FileOutputStream(localFile);
				os.write(uploaded.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			int id = miAnunciosDAO.registrarAnuncio(nuevoAnuncio);
			localFile.renameTo(new File("C://subidasJava/" + id + ".jpg"));
			System.out.println("anuncio registrado ok");
			return controladorInicio.inicio(model,"",0);
		}
	}// end guardarAnuncio

}// end class
