package controladores;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daos.ServiciosDAO;
import modelo.Anuncio;
import modelo.Servicio;

@Controller
public class ControlladorServiciosInicio {
	
	@Autowired
	private ServiciosDAO serviciosDAO;
	
	@RequestMapping("serviciosInicio")
	public String serviciosInicio(Map model, String campoBusqueda, Integer comienzo) {
		System.out.println("se ejecuta el metodo serviciosInicio");
		
		if(comienzo == null) {
			comienzo = 0;
		}
		if(campoBusqueda == null) {
			campoBusqueda = "";
		}
		
		int cuantos = 5;
		int siguiente = comienzo + cuantos;
		int anterior = comienzo - cuantos;
		
		model.put("anterior", anterior);
		model.put("siguiente", siguiente);
		model.put("campoBusqueda", campoBusqueda);
		
		List<Servicio> servicios = serviciosDAO.obtenerServiciosPorTituloIndicandoComienzoYCunatos(campoBusqueda, comienzo, cuantos);//obtenerServicios();
		model.put("servicios", servicios);
		// Lo siguiente lleva a la vista inicio.jsp
		// de las vistas gracias a la bean que acabammos de insertar en el 
		// dispatcher-servlet.xml
		return "servicios";
	}
	
	@RequestMapping("/detallesServicio")
	public String detallesServicio(int id, Map model) {
		
		Servicio servicio = serviciosDAO.obtenerServicioPorId(id);
		model.put("servicio", servicio);
		return "detallesServicio";
	}
	
}
