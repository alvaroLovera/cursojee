package constantes;

public class ConstantesSQL {
	
	public static final String SQL_OBTENER_USARIO_POR_EMAIL_Y_PASS = "select * from tabla_usuarios2 where email = ? AND pass = ? ;";
	public static final String SQL_BORRAR_ANUNCIO = "DELETE FROM tabla_anuncios where id = ?;";
	public static final String SQL_OBTENER_ANUNCIO_POR_ID = "SELECT * FROM tabla_anuncios where id= ?;";
	public static final String SQL_ACTUALIZAR_ANUNCIO = "UPDATE tabla_anuncios set titulo = ? , email = ?, precio = ?, telefono = ?, ciudad = ?, estado = ?, descripcion = ? where id = ?";
	public static final String SQL_BORRAR_SERVICIO = "DELETE FROM tabla_servicios where id = ?;";
	public static final String SQL_OBTENER_SERVICIO_POR_ID = "SELECT * FROM tabla_servicios where id= ?;";
	public static final String SQL_ACTUALIZAR_SERVICIO = "UPDATE tabla_servicios set nombre = ?, profesion = ?, tarea = ?, descripcion = ?, ciudad = ? where id = ?; ";
//	public static final String SQL_OBTENER_ANUNCIOS = "select * from tabla_anuncios order by id desc ;";
	public static final String SQL_OBTENER_ANUNCIOS = "select * from tabla_anuncios WHERE titulo like '%' ? '%' ;";
	public static final String SQL_OBTENER_SERVICIOS = "select * from tabla_servicios order by id desc ;";
	public static final String SQL_OBTENER_USUARIOS = "select * from tabla_usuarios2 order by id desc ;";
	public static final String SQL_BORRAR_USUARIO = "DELETE FROM tabla_usuarios2 where id = ?;";
	public static final String SQL_OBTENER_USUARIO_POR_ID = "SELECT * FROM tabla_usuarios2 where id= ?;";
	public static final String SQL_ACTUALIZAR_USUARIO = "UPDATE tabla_usuarios2 set nombre = ?, email = ?, pass = ? where id = ?; ";
	public static final String SQL_OBTENER_ANUNCIOS_INDICANDO_COMIENZA_CUANTOS = "select * from tabla_anuncios WHERE titulo like '%' ? '%' order by id asc limit ? , ? ;";
	public static final String SQL_OBTENER_ANUNCIOS_INDICANDO_COMIENZO_Y_CUANTOS = "SELECT * FROM tabla_anuncios WHERE titulo like ? order by id desc LIMIT ? , ? ;";
	public static final String SQL_OBTENER_SERVICIOS_INDICANDO_COMIENZO_Y_CUANTOS = "SELECT * FROM tabla_servicios WHERE nombre like ? order by id desc LIMIT ? , ? ;";
}
