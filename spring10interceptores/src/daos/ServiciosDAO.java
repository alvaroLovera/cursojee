package daos;

import java.util.List;

import modelo.Servicio;


public interface ServiciosDAO {
	public int registrarServicio(Servicio s);
	public List<Servicio> obtenerServicios();
	public void borrarServicio(int id);
	public Servicio obtenerServicioPorId(int id);
	public void actualizarServicio(Servicio servicioAeditar);
	public List<Servicio> obtenerServiciosPorTituloIndicandoComienzoYCunatos(String campoBusqueda, Integer comienzo,
			int cuantos);
}
