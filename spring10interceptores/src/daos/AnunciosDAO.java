package daos;

import java.util.List;

import modelo.Anuncio;

public interface AnunciosDAO {

	public int registrarAnuncio(Anuncio a);
	public List<Anuncio> obntenerAnuncios(String titulo);
	public void borrarAnuncio(int id);
	public Anuncio obtenerAnuncioPorId(int id);
	public void actualizarAnuncio(Anuncio anuncioAeditar);
	public List<Anuncio> obtenerAnunciosIndicandoComienzoYcuantos(String titulo, int comienzo, int cuantos);
	public List<Anuncio> obtenerAnunciosPorTituloIndicandoComienzoYCunatos(String campoBusqueda, Integer comienzo,
			int cuantos);
}
