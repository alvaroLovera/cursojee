package daos;

import java.util.List;

import modelo.Usuario;

public interface UsuariosDAO {
	
	public void registrarUsuario(Usuario u);
	public Usuario obtenerUsuarioPorMailYpass(String email, String pass);
	public List<Usuario> obtenerUsuarios();
	public void borrarUsuario(int id);
	public Usuario obtenerUsuarioPorId(int id);
	public void actualizarUsuario(Usuario usuarioAeditar);
}
