package modelo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class Servicio {

	private int id;
	@NotEmpty(message = "Rellene el campo nombre")
	private String nombre;
	@NotEmpty(message = "Rellene el campo profesion")
	private String profesion;
	@NotEmpty(message = "Rellene el campo tarea")
	private String tarea;
	@NotEmpty(message = "El campo descripcion debe tener minimo 3 caracteres y maximo 300")
	@Size(max = 300, min = 1)
	private String descripcion;
	@NotEmpty(message = "Rellene el campo ciudad")
	private String ciudad;
	
	private boolean alta;
	
	
	private CommonsMultipartFile fichero1;
	private CommonsMultipartFile fichero2;
	private CommonsMultipartFile fichero3;
	
	public Servicio() {
	}

	

	public Servicio(String nombre, String profesion, String tarea, String descripcion, String ciudad, boolean alta) {
		super();
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
		this.alta = alta;
	}

	
	public Servicio(int id, String nombre, String profesion, String tarea, String descripcion, String ciudad,
			boolean alta) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.profesion = profesion;
		this.tarea = tarea;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
		this.alta = alta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getTarea() {
		return tarea;
	}

	public void setTarea(String tarea) {
		this.tarea = tarea;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	

	public CommonsMultipartFile getFichero1() {
		return fichero1;
	}

	public void setFichero1(CommonsMultipartFile fichero1) {
		this.fichero1 = fichero1;
	}

	public CommonsMultipartFile getFichero2() {
		return fichero2;
	}

	public void setFichero2(CommonsMultipartFile fichero2) {
		this.fichero2 = fichero2;
	}

	public CommonsMultipartFile getFichero3() {
		return fichero3;
	}

	public void setFichero3(CommonsMultipartFile fichero3) {
		this.fichero3 = fichero3;
	}

	public boolean isAlta() {
		return alta;
	}

	public void setAlta(boolean alta) {
		this.alta = alta;
	}

	@Override
	public String toString() {
		return "Servicio [id=" + id + ", nombre=" + nombre + ", profesion=" + profesion + ", tarea=" + tarea
				+ ", descripcion=" + descripcion + ", ciudad=" + ciudad + ", alta=" + alta + "]";
	}

	
	
	
}
