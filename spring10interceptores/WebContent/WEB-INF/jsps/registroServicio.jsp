<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" 
prefix="springform"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
Inserta los datos de tu servicio:
<springform:form method="post" enctype="multipart/form-data" action="guardarServicio" commandName="nuevoServicio">
	<span style="color: red;" ><springform:errors path="nombre" /></span>
	nombre: <springform:input path="nombre"/><br/>
	<span style="color: red;" ><springform:errors path="profesion" /></span>
	profesion: <springform:input path="profesion"/><br/>
	<span style="color: red;" ><springform:errors path="descripcion" /></span>
	descripcion: <br/>
		<span style="color: red;" ><springform:textarea path="descripcion" cols="20" 
		rows="3"/><br/></span>
	<span style="color: red;" ><springform:errors path="tarea" /></span>
	tarea:	<springform:input path="tarea"/><br/>
	<span style="color: red;" ><springform:errors path="ciudad" /></span>
	Ciudad:	<springform:input path="ciudad"/><br/>
	<input type="file" name="fichero1"><br/>
	<input type="file" name="fichero2"><br/>
	<input type="file" name="fichero3"><br/>
	<input type="submit" value="REGISTRAR MI Servicio"/>

</springform:form>
</body>
</html>