<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" 
prefix="springform"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

Inserta los datos de tu anuncio:
<springform:form method="post" enctype="multipart/form-data" action="guardarAnuncio" commandName="nuevoAnuncio">
	<span style="color: red;" ><springform:errors path="titulo" /></span>
	titulo: <springform:input path="titulo"/><br/>
	<span style="color: red;" ><springform:errors path="email" /></span>
	email: <springform:input path="email"/><br/>
	<span style="color: red;" ><springform:errors path="descripcion" /></span>
	descripcion: <br/>
		<span style="color: red;" ><springform:textarea path="descripcion" cols="20" 
		rows="3"/><br/></span>
		<span style="color: red;" ><springform:errors path="precio" /></span>
	Precio:	<springform:input path="precio"/><br/>
	<span style="color: red;" ><springform:errors path="estado" /></span>
	Estado:<br/>
	<springform:radiobutton path="estado" value="Segunda Mano"/>Segunda Mano<br/>
	<springform:radiobutton path="estado" value="Sin usar"/>Sin usar<br/>
	<springform:radiobutton path="estado" value="Seminuevo"/>Seminuevo<br/>
	
	
	<span style="color: red;" ><springform:errors path="telefono" /></span>
	Tlf:<springform:input path="telefono"/><br/>
	<span style="color: red;" ><springform:errors path="ciudad" /></span>
	Ciudad:
<%-- 		<springform:input path="ciudad"/><br/> --%>
	<springform:select path="ciudad">
		<springform:option value="Madrid">Madrid</springform:option>
		<springform:option value="Cordoba">C�rdoba</springform:option>
		<springform:option value="Alicante">Alicante</springform:option>
		<springform:option value="Albacete">Albacete</springform:option>
	</springform:select><br/>
<!-- 	<input type="file" name="imagen" /> <br/> -->
		<input type="file" name="fichero"><br/>
	<input type="submit" value="REGISTRAR MI ANUNCIO"/>

</springform:form>

</body>
</html>