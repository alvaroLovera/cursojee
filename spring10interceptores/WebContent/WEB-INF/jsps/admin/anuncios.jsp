<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
 <%@ taglib uri="http://www.springframework.org/tags/form" 
prefix="springform"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<form id="divBuscador" class="estiloBuscador" action="buscarAnuncio" >
	<spring:message code="buscar"/>
	<input type="text" name="nombreBusquedaAnuncio" />
	<input type="submit"  />
	
</form>


<div id="divEnlacesPaginacion" class="estiloPaginacion" >
<%-- 		<core:if test="${anterior >= 0}"> --%>
			<a href="?comienzo=${anterior}">Anterior</a>
<%-- 		</core:if> --%>
		Total Resultados : ${totalResultados}
<%-- 		<core:if test="${siguiente < totalResultados}">	 --%>
			<a href="?comienzo=${siguiente}">Siguiente</a>
<%-- 		</core:if> --%>
	</div>
<table>
<tr>
	<th>Imagenes</th>
	<th>Titulo</th>
	<th>Email</th>
	<th>Descripcion</th>
</tr>
<core:forEach items="${anuncios}" var="elemento" >
<tr>
<td><img height="150px" src="C:/subidasJava/${elemento.id}.jpg" /></td>
	<td>${elemento.titulo}</td>
	<td>${elemento.email}</td>
	<td>${elemento.descripcion}</td>
	<td><a href="borrarAnuncio?id=${elemento.id}" >BORRAR</a></td>
	<td><a href="editarAnuncio?id=${elemento.id}" >EDITAR</a></td>
</tr>
</core:forEach>

</table>

</body>
</html>