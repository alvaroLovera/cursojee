<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<jsp:include page="menu.jsp"/>

<form id="divBuscador" class="estiloBuscador" action="buscarAnuncio" >
	<spring:message code="buscar"/>
	<input type="text" name="nombreBusquedaAnuncio" />
	<input type="submit"  />
</form>


<table>
<tr>
	<th>Imagen</th>
	<th>Nombre</th>
	<th>Profesion</th>
	<th>Tarea</th>
</tr>
<core:forEach items="${servicios}" var="elemento" >
<tr>
	<td><img height="150px" src="C:/subidasJava/${elemento.id}.jpg" /></td>
	<td>${elemento.nombre}</td>
	<td>${elemento.profesion}</td>
	<td>${elemento.tarea}</td>
	<td><a href="borrarServicio?id=${elemento.id}" >BORRAR</a></td>
	<td><a href="editarServicio?id=${elemento.id}" >EDITAR</a></td>
</tr>
</core:forEach>

</table>

</body>
</html>