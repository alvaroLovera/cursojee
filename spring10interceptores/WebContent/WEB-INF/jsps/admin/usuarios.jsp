<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<table>
<tr>
	<th>Nombre</th>
	<th>Email</th>
	<th>Pass</th>
</tr>
<core:forEach items="${usuarios}" var="elemento" >
<tr>
	<td>${elemento.nombre}</td>
	<td>${elemento.email}</td>
	<td>${elemento.pass}</td>
	<td><a href="borrarUsuario?id=${elemento.id}" >BORRAR</a></td>
	<td><a href="editarUsuario?id=${elemento.id}" >EDITAR</a></td>
</tr>
</core:forEach>

</table>
</body>
</html>