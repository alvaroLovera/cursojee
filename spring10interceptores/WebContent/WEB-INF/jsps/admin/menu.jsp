<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<core:set var="rutaBase" value="${pageContext.request.contextPath }" />
<div>
	<a href="${rutaBase}/admin/anuncios">Gesionar Anuncios</a>
	<a href="${rutaBase}/admin/servicios">Gesionar Servicios</a>
	<a href="${rutaBase}/admin/usuarios">Gesionar Usuarios</a>
</div>