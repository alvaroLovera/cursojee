<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" 
prefix="springform"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
Inserta los datos de tu servicio a modificar:
<springform:form method="post" enctype="multipart/form-data" action="actualizarServicio" commandName="servicioAeditar">
	<span style="color: red;" ><springform:errors path="nombre" /></span>
	nombre: <springform:input path="nombre"/><br/>
	<span style="color: red;" ><springform:errors path="profesion" /></span>
	profesion: <springform:input path="profesion"/><br/>
	
	descripcion:<span style="color: red;" ><springform:errors path="descripcion" /></span><br/>
	 <springform:textarea cols="20" rows="3" path="descripcion"/><br/>
	 
	<span style="color: red;" ><springform:errors path="tarea" /></span>
	Tarea:	<springform:input path="tarea"/><br/>
	<span style="color: red;" ><springform:errors path="ciudad" /></span>
	Ciudad:
	<springform:select path="ciudad">
		<springform:option value="Madrid">Madrid</springform:option>
		<springform:option value="Cordoba">C�rdoba</springform:option>
		<springform:option value="Alicante">Alicante</springform:option>
		<springform:option value="Albacete">Albacete</springform:option>
	</springform:select><br/>
	<img src="C:/subidasJava/${servicioAeditar.id}.jpg" />
	<input type="file" name="fichero"><br/>
	<springform:hidden path="id"/>
	<input type="submit" value="Guardar Cambios"/>

</springform:form>
</body>
</html>