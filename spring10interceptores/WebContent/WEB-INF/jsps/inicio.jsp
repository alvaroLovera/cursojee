<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
	.anuncio {
		margin: 10px;
		padding: 5px;
	
	}

</style>
<title>Insert title here</title>
</head>
<body>
<div style="text-align: center; size: 20px">
	<spring:message code="titulo"/>
</div>
<div style="text-align: center" >
	<a href="?locale=en">Ingles</a>
	<a href="?locale=es">Espa�ol</a>
</div>
<c:if test="${ sessionScope.idUsuario != null }">
	<a href="prepararRegistro">REGISTRAR MI ANUNCIO</a>
</c:if>
<c:if test="${ sessionScope.idUsuario == null }">
	(para registrar tu anuncio identificate/registrate)
</c:if>

<a href="login">Identificarme</a>
<a href="registrarUsuario">REGISTRAR Usuario</a>
<br/>
<a href="serviciosInicio" >ACCEDER A Servicios</a>
<a href="serviciosInicio" >Portal Servicios</a>
Listado de anuncios:<br/>

<form action="">
titulo: <input type="search" name="campoBusqueda" value="${campoBusqueda }" />
<input type="submit" value="Buscar"  >
</form>
<div>
<!-- la ? no va delante a ningun sitio porque se manda a si mismo -->
<%-- <c:if test="${anterior} >= 0"> --%>
	<a href="?comienzo=${anterior}&campoBusqueda=${campoBusqueda}">Anterior</a>
<%-- </c:if> --%>
<%-- <c:if test="${siguiente} > 0"> --%>
	<a href="?comienzo=${siguiente}&campoBusqueda=${campoBusqueda}">Siguiente</a>
<%-- 	</c:if> --%>
</div>
<c:forEach items="${anuncios}" var="anuncio" >
<div class="anuncio" >
-------------------------------<br/>
Titulo: ${anuncio.titulo} <br/>
<img height="150px" src="C:/subidasJava/${anuncio.id}.jpg" /><br/>
Descripcion:  ${anuncio.descripcion} <br/>
Email:  ${anuncio.email}<br/>
Precio: ${anuncio.precio}<br/>
Estado: ${anuncio.estado}<br/>
Tlf: ${anuncio.telefono}<br/>
Ciudad: ${anuncio.ciudad}<br/>
-------------------------------<br/>
</div>
</c:forEach>

</body>
</html>