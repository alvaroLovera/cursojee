<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
  background-color: linen;
}
* {
  box-sizing: border-box;
}
[class*="col-"] {
  float: left;
  padding: 15px;
  border: 1px solid red;
}
.row::after {
  content: "";
  clear: both;
  display: table;
}
</style>
</head>
<body>
<c:if test="${ sessionScope.idUsuario != null }">
	<a href="prepararServicio">REGISTRAR MI Servicio</a>
</c:if>
<a href="index.jsp" >Inicio</a>
<br/>
<c:if test="${ sessionScope.idUsuario == null }">
	(para registrar tu anuncio identificate/registrate)
</c:if>

<a href="login">Identificarme</a>
<a href="registrarUsuario">REGISTRAR Usuario</a>
<br/>
<form action="">
titulo: <input type="search" name="campoBusqueda" value="${campoBusqueda }" />
<input type="submit" value="Buscar"  >
</form>
<div>
<!-- la ? no va delante a ningun sitio porque se manda a si mismo -->
<%-- <c:if test="${anterior} >= 0"> --%>
	<a href="?comienzo=${anterior}&campoBusqueda=${campoBusqueda}">Anterior</a>
<%-- </c:if> --%>
<%-- <c:if test="${siguiente} > 0"> --%>
	<a href="?comienzo=${siguiente}&campoBusqueda=${campoBusqueda}">Siguiente</a>
<%-- 	</c:if> --%>
</div>
Listado de servicios:<br/>
<%-- <c:forEach var = "i" begin = "1" end = "5"> --%>
<%-- <c:if test="i%3 == 0"> --%>
<%-- <div class="row-${i=i+1}"> --%>
	
<%-- 		<c:forEach items="${servicios}" var="servicio" >		  --%>
<!-- 				<div class="col-1" > -->
<!-- 					-------------------------------<br/> -->
<%-- 					Nombre: ${servicio.nombre} <br/> --%>
<%-- 					<img height="150px" src="C:/subidasJava/${servicio.id}.jpg" /><br/> --%>
<%-- 					Profesion: ${servicio.profesion} <br/> --%>
<%-- 					Tarea:  ${servicio.tarea}<br/> --%>
<%-- 					Descripcion: ${servicio.descripcion}<br/> --%>
<%-- 					Ciudad: ${servicio.ciudad}<br/> --%>
<!-- 					-------------------------------<br/> -->
<!-- 				</div> -->
<%-- 		</c:forEach>		 --%>
<!-- </div> -->
<%-- 	</c:if>	 --%>
<%-- 	<c:if test="i%3 != 0">	 --%>
<%-- 		<div class="row-${i}"> --%>
<%-- 			<c:forEach items="${servicios}" var="servicio" >		  --%>
<!-- 					<div class="col-1" > -->
<!-- 						-------------------------------<br/> -->
<%-- 						Nombre: ${servicio.nombre} <br/> --%>
<%-- 						<img height="150px" src="C:/subidasJava/${servicio.id}.jpg" /><br/> --%>
<%-- 						Profesion: ${servicio.profesion} <br/> --%>
<%-- 						Tarea:  ${servicio.tarea}<br/> --%>
<%-- 						Descripcion: ${servicio.descripcion}<br/> --%>
<%-- 						Ciudad: ${servicio.ciudad}<br/> --%>
<!-- 						-------------------------------<br/> -->
<!-- 					</div> -->
<%-- 			</c:forEach>		 --%>
<!-- 		</div> -->
<%-- 	</c:if>	 --%>
<%-- </c:forEach> --%>


<c:forEach items="${servicios}" var="servicio" >
-------------------------------<br/>
<div class="servicio-box">	
	<a href="detallesServicio?id=${servicio.id}" >	
		Nombre: ${servicio.nombre} <br/>
		<img height="150px" src="C:/subidasJava/${servicio.id}a.jpg" /><br/>
	</a>
</div>
</c:forEach>
</body>
</html>