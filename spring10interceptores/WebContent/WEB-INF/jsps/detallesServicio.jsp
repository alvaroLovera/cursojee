<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" 
prefix="springform"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>
</head>
<body>
<springform:form commandName="servicio" >
	nombre: <springform:input readonly="readonly" path="nombre"/><br/>
	
	profesion: <springform:input readonly="readonly"  path="profesion"/><br/>
	
	descripcion:<br/>
	 <springform:textarea readonly="readonly" path="descripcion"  cols="20" rows="3" /><br/>
	 

	Tarea:	<springform:input readonly="readonly"  path="tarea"/><br/>

	Ciudad:
	<springform:input readonly="readonly"  path="ciudad"/><br/>
	
<%-- 	<img src="C:/subidasJava/${servicio.id}a.jpg" /> --%>
	<springform:hidden path="id"/>
</springform:form>

<div class="w3-content w3-display-container">
  <img class="mySlides" src="C:/subidasJava/${servicio.id}a.jpg" style="height: 150px; width:20%">
  <img class="mySlides" src="C:/subidasJava/${servicio.id}b.jpg" style="height: 150px; width:20%">
  <img class="mySlides" src="C:/subidasJava/${servicio.id}c.jpg" style="height: 150px; width:20%">

  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

</body>
</html>