package beans;

public class GestorArchivos {
	// Para poder usar un gestor de imagenes desde este gestor de archivos 
	// se hace asi
	private GestorImagenes gestorImagenes;
	private String rutaRaiz;

	public void borrarArchivo(String archivo) {
		System.out.println("Acrtuand sobre " + rutaRaiz);
		System.out.println("Borrando archivo: " + archivo);
		System.out.println("Borrando imagenes del archivo");
		gestorImagenes.borrarImagenesDeArchivo(archivo);
	}
	
	public String getRutaRaiz() {
		return rutaRaiz;
	}

	public void setRutaRaiz(String rutaRaiz) {
		this.rutaRaiz = rutaRaiz;
	}

	public GestorImagenes getGestorImagenes() {
		return gestorImagenes;
	}

	public void setGestorImagenes(GestorImagenes gestorImagenes) {
		this.gestorImagenes = gestorImagenes;
	}

	
}
