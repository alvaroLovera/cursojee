package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.GestorArchivos;


public class Principal {

	public static void main(String[] args) {
		
		ApplicationContext contenedor = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		
		// El valor de la raiz a sido especificada en el xml
		// eso es lo que se llama asignar una propiedad a una Bean
			GestorArchivos ga =
					contenedor.getBean(GestorArchivos.class);
			ga.borrarArchivo("imagen.jpg");
	}

}
